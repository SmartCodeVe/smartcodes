@extends('admin.layout.base')

@section('title', 'Confic 3 ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

			<h5 style="margin-bottom: 2em;">@lang('admin.exchange.confic3')</h5>

		</div>
    </div>
</div>

@endsection
