@extends('admin.layout.base')

@section('title', 'Confic 4 ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

			<h5 style="margin-bottom: 2em;">@lang('admin.exchange.confic4')</h5>

		</div>
    </div>
</div>

@endsection
