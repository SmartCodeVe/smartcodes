@extends('admin.layout.base')

@section('title', 'Confic 1 ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">

			<h5 style="margin-bottom: 2em;">@lang('admin.exchange.confic1')</h5>
			
			<form class="form-horizontal" action="{{ route('admin.settings.store') }}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}

				<div class="form-group row">
					<label for="broadcast_request" class="col-xs-2 col-form-label"> Comprar </label>
					<div class="col-xs-10">
						<div class="float-xs-left mr-1"><input @if(Setting::get('broadcast_request') == 1) checked  @endif  name="broadcast_request" type="checkbox" class="js-switch" data-color="#43b968"></div>
					</div>
				</div>

				<div class="form-group row">
					<label for="track_distance" class="col-xs-2 col-form-label"> Vender </label>
					<div class="col-xs-10">
						<div class="float-xs-left mr-1"><input @if(Setting::get('track_distance') == 1) checked  @endif  name="track_distance" type="checkbox" class="js-switch" data-color="#43b968"></div>
					</div>
				</div>
				
			</form>

		</div>
    </div>
</div>

@endsection
