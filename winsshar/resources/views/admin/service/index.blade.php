@extends('admin.layout.base')

@section('title', 'Tipos de Servicios ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Tipos de Servicios</h5>
            <a href="{{ route('admin.service.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>@lang('admin.service.Add_Service_Type')</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Activo</th>
                        <th>Icono</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($bussinesTypes as $index => $bussinesType)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $bussinesType->name }}</td>

                        <td>@if( $bussinesType->active == 1)
                                <label class="btn btn-primary">@lang('admin.yes')</label>
                            @else
                                <label class="btn btn-warning">@lang('admin.no')</label>
                            @endif
                        </td>
                        <td><img style="width:50px;" src="{{asset('images/'.$bussinesType->icon)}}" ></td>
                        <td>
                            <form action="{{ route('admin.service.destroy', $bussinesType->id) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <a href="{{ route('admin.service.edit', $bussinesType->id) }}" class="btn btn-info btn-block">
                                    <i class="fa fa-pencil"></i>@lang('admin.edit')
                                </a>
                                @if(!$bussinesType->by_default)
                                    <button class="btn btn-danger btn-block" onclick="return confirm('Esta seguro(a)?')">
                                        <i class="fa fa-trash"></i> @lang('admin.delete')
                                    </button>
                                @endif
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Activo</th>
                        <th>Icono</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection