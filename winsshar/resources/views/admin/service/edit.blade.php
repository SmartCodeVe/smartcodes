@extends('admin.layout.base')

@section('title', 'Update Service Type ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.service.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> @lang('admin.back')</a>

            <h5 style="margin-bottom: 2em;">@lang('admin.service.Update_User')</h5>

            <form class="form-horizontal" action="{{route('admin.service.update', $service->id )}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                

                   <div class="form-group row">
                    <label for="name" class="col-xs-12 col-form-label">@lang('admin.service.Service_Name')</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->name }}" name="name" required id="name" placeholder="Service Name">
                    </div>
                </div>

              
                <div class="form-group row">
                    <label for="active" class="col-xs-12 col-form-label">@lang('admin.service.service_active')</label>
                    <div class="col-xs-10">
                        


                       <select id='active' name='active' class="form-control">
                          <option value='1' {{($service->active)=="1" ? 'selected':''}}> @lang('admin.service.service_active')</option>
                          <option value='0' {{($service->active)=="0" ? 'selected':''}}> @lang('admin.service.service_inactive')</option>
                       </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="picture" class="col-xs-12 col-form-label">
                    @lang('admin.service.Service_Image')</label>
                    @if(isset($service->icon))
                        <img style="height: 90px; margin-bottom: 15px; border-radius:2em;" src="{{asset('images/'.$service->icon )}}">
                        
                        @endif
                    <div class="col-xs-10">
                        <input type="file" accept="image/*" name="icon" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="{{route('admin.service.index')}}" class="btn btn-danger btn-block">@lang('admin.cancel')</a>
                    </div>
                    <div class="col-xs-12 col-sm-6 offset-md-6 col-md-3">
                        <button type="submit" class="btn btn-primary btn-block">@lang('admin.service.Update_Service_Type')</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection