@extends('admin.layout.base')

@section('title', 'Pagos ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
                <h5 class="mb-1">@lang('admin.payment.payment_history')</h5>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>@lang('admin.payment.request_id')</th>
                            <th>@lang('admin.payment.transaction_id')</th>
                            <th>@lang('admin.payment.from')</th>
                            <th>@lang('admin.payment.to')</th>
                            <th>@lang('admin.payment.total_amount')</th>
                            <th>@lang('admin.payment.comission')</th>
                            <th>@lang('admin.payment.date_registered')</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $index => $payment)
                        <tr>
                            <td>{{$payment->id}}</td>
                            <td>{{$payment->transaction_id}}</td>
                            <td>{{$payment->customer->name}}</td>
                            <td>{{$payment->userBussiness->user->name}}</td>
                            <td>{{currency($payment->total_amount)}}</td>
                            <td>{{currency($payment->total_comission)}}</td>
                            <td><span class="text-muted">{{$payment->created_at->diffForHumans()}}</span></td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>@lang('admin.payment.request_id')</th>
                            <th>@lang('admin.payment.transaction_id')</th>
                            <th>@lang('admin.payment.from')</th>
                            <th>@lang('admin.payment.to')</th>
                            <th>@lang('admin.payment.total_amount')</th>
                            <th>@lang('admin.payment.comission')</th>
                            <th>@lang('admin.payment.date_registered')</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection