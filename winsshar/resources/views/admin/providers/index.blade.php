@extends('admin.layout.base')

@section('title', 'Negocios ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                @lang('admin.include.bussiness')
            </h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>@lang('admin.id')</th>
                        <th>@lang('admin.name')</th>
                        <th>@lang('admin.email')</th>
                        <th>@lang('admin.mobile')</th>
                        <th>@lang('admin.include.service_type')</th>
                        <th>@lang('admin.include.active')</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($providers as $index => $provider)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $provider->name }} </td>
                        <td>{{ $provider->user->email }}</td>
                        <td>{{ $provider->user->mobile }}</td>
                        <td style="text-align:center;"><img style="width:50px;" src="{{asset('images/'.$provider->bussinesType->icon)}}" ></td>
                        <td>
                            @if($provider->active == '1')
                                <label class="btn btn-block btn-primary">@lang('admin.yes')</label>
                            @else
                                <label class="btn btn-block btn-warning">@lang('admin.no')</label>
                            @endif
                        </td>
                        <td>
                            <div class="input-group-btn">
                                @if($provider->active == '1')
                                    <a class="btn btn-danger btn-block" href="{{ route('admin.provider.disapprove', $provider->id ) }}">@lang('admin.Disable')</a>
                                @else
                                    <a class="btn btn-success btn-block" href="{{ route('admin.provider.approve', $provider->id ) }}">@lang('admin.Enable')</a>
                                @endif 
                                <form action="{{ route('admin.provider.destroy', $provider->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button class="btn btn-default btn-block" onclick="return confirm('Esta seguro(a)?')"><i class="fa fa-trash"></i>@lang('admin.delete')</button>
                                </form>
                                
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>@lang('admin.id')</th>
                        <th>@lang('admin.name')</th>
                        <th>@lang('admin.email')</th>
                        <th>@lang('admin.mobile')</th>
                        <th>@lang('admin.include.service_type')</th>
                        <th>@lang('admin.include.active')</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection