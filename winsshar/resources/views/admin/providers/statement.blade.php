@extends('admin.layout.base')

@section('title', $page)

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            <div class="box box-block bg-white">
            	<h3>{{$page}}</h3>

            	<div style="text-align: center;padding: 20px;color: blue;font-size: 24px;">
            		<p><strong>
            			<span>Ganancias : {{currency($revenue[0]->overall)}}</span>
            		</strong></p>
            	</div>

            	<div class="row">

	            	<div class="col-lg-4 col-md-6 col-xs-12">
						<div class="box box-block bg-white tile tile-1 mb-2">
							<div class="t-icon right"><span class="bg-danger"></span><i class="ti-rocket"></i></div>
							<div class="t-content">
								<h6 class="text-uppercase mb-1">Total No. de Viajes</h6>
								<h1 class="mb-1">{{$rides->count()}}</h1>
							</div>
						</div>
					</div>


					<div class="col-lg-4 col-md-6 col-xs-12">
						<div class="box box-block bg-white tile tile-1 mb-2">
							<div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
							<div class="t-content">
								<h6 class="text-uppercase mb-1">Ganancias</h6>
								<h1 class="mb-1">{{currency($revenue[0]->overall)}}</h1>
								<i class="fa fa-caret-up text-success mr-0-5"></i><span>de {{$rides->count()}} Viajes</span>
							</div>
						</div>
					</div>

					<div class="col-lg-4 col-md-6 col-xs-12">
						<div class="box box-block bg-white tile tile-1 mb-2">
							<div class="t-icon right"><span class="bg-warning"></span><i class="ti-archive"></i></div>
							<div class="t-content">
								<h6 class="text-uppercase mb-1">Viajes Cancelados</h6>
								<h1 class="mb-1">{{$cancel_rides}}</h1>
							</div>
						</div>
					</div>

						<div class="row row-md mb-2" style="padding: 15px;">
							<div class="col-md-12">
									<div class="box bg-white">
										<div class="box-block clearfix">
											<h5 class="float-xs-left">Ganancias</h5>
											<div class="float-xs-right">
											</div>
										</div>

										@if(count($rides) != 0)
								            <table class="table table-striped table-bordered dataTable" id="table-2">
								                <thead>
								                   <tr>
														<td>ID</td>
														<td>Origen</td>
														<td>Destino</td>
														<td>Fecha</td>
														<td>Estatus</td>
														<td>Monto</td>
													</tr>
								                </thead>
								                <tbody>
								                <?php $diff = ['-success','-info','-warning','-danger']; ?>
														@foreach($rides as $index => $ride)
															<tr>
																<td>{{$ride->id}}</td>
																<td>
																	@if($ride->pickup_address != '')
																		{{$ride->pickup_address}}
																	@else
																		--
																	@endif
																</td>
																<td>
																	@if($ride->drop_address != '')
																		{{$ride->drop_address}}
																	@else
																		--
																	@endif
																</td>
																<td>
																	<span class="text-muted">{{date('d M Y',strtotime($ride->created_at))}}</span>
																</td>
																<td>
																	@if($ride->status == "end")
																		<span class="tag tag-success">Compleatado</span>
																	@elseif($ride->status == "cancelado")
																		<span class="tag tag-danger">Cancelado</span>
																	@else
																		<span class="tag tag-info">En Proceso</span>
																	@endif
																</td>
																<td>{{currency($ride->total_price)}}</td>
 														</tr>
														@endforeach
															
								                <tfoot>
								                    <tr>
														<td>ID</td>
														<td>Origen</td>
														<td>Destino</td>
														<td>Fecha</td>
														<td>Estatus</td>
														<td>Monto</td>
													</tr>
								                </tfoot>
								            </table>
								            @else
								            	<h6 class="no-result">No hay registros</h6>
								            @endif 

									</div>
								</div>

							</div>

            	</div>

            </div>
        </div>
    </div>

@endsection
