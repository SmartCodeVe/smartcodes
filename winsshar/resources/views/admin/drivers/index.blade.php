@extends('admin.layout.base')

@section('title', 'Conductores ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                @lang('admin.include.drivers')
            </h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>@lang('admin.id')</th>
                        <th>@lang('admin.name')</th>
                        <th>@lang('admin.email')</th>
                        <th>@lang('admin.mobile')</th>
                        <th>@lang('admin.drivers.accepted_requests')</th>
                        <th>@lang('admin.drivers.cancelled_requests')</th>
                        <th>@lang('admin.drivers.total_requests')</th>
                        <th>@lang('admin.include.service_type')</th>
                        <th>@lang('admin.drivers.status')</th>
                        <th>@lang('admin.drivers.driver_documents')</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($drivers as $index => $driver)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $driver->userBussiness->name }} </td>
                        <td>{{ $driver->userBussiness->user->email }}</td>
                        <td>{{ $driver->userBussiness->user->mobile }}</td>
                        <td style="text-align:center;">{{ $driver->acceptedRequest() }}</td>
                        <td style="text-align:center;">{{ $driver->cancelledRequest() }}</td>
                        <td style="text-align:center;">{{ $driver->acceptedRequest() + $driver->cancelledRequest() }}</td>
                        <td style="text-align:center;"><img style="width:50px;" src="{{asset('images/'.$driver->userBussiness->bussinesType->icon)}}" ></td>
                        <td>
                            @if($driver->proof_status == 'Accepted')
                                <label class="btn btn-block btn-primary">@lang('admin.drivers.Approved')</label>
                            @elseif($driver->proof_status == 'Pending')
                            <label class="btn btn-block btn-warning">@lang('admin.drivers.Pending')</label>
                            @else
                                <label class="btn btn-block btn-danger">@lang('admin.drivers.Not_Approved')</label>
                            @endif
                        </td>
                        <td>
                        <a class="btn btn-success btn-block" href="{{route('admin.provider.document.index', $driver->id )}}">Ver</a>
                        </td>
                        <td>
                            <div class="input-group-btn">
                                @if($driver->proof_status == 'Accepted' || $driver->proof_status == 'Pending')
                                    <a class="btn btn-danger btn-block" href="{{ route('admin.provider.disapprove', $provider->id ) }}">@lang('admin.Disable')</a>
                                @else
                                    <a class="btn btn-success btn-block" href="{{ route('admin.provider.approve', $provider->id ) }}">@lang('admin.Enable')</a>
                                @endif                                
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>@lang('admin.id')</th>
                        <th>@lang('admin.name')</th>
                        <th>@lang('admin.email')</th>
                        <th>@lang('admin.mobile')</th>
                        <th>@lang('admin.drivers.accepted_requests')</th>
                        <th>@lang('admin.drivers.cancelled_requests')</th>
                        <th>@lang('admin.drivers.total_requests')</th>
                        <th>@lang('admin.include.service_type')</th>
                        <th>@lang('admin.drivers.status')</th>
                        <th>@lang('admin.drivers.driver_documents')</th>
                        <th>@lang('admin.action')</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection