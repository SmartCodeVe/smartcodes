@extends('admin.layout.base')

@section('title', 'Provider Reviews ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">@lang('admin.review.Driver_Reviews')</h5>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>@lang('admin.id')</th>
                            <th>@lang('admin.request.Driver_Name')</th>
                            <th>@lang('admin.request.User_Name')</th>
                            <th>@lang('admin.review.Rating')</th>
                            <th>@lang('admin.request.Date_Time')</th>
                            <!--<th>@lang('admin.review.Comments')</th> -->
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($Reviews as $index => $review)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$review->driver->userBussiness->user->name}}</td>
                            <td>{{$review->user->name}}</td>
                            <td>
                                <div className="rating-outer">
                                    <input type="hidden" value="{{$review->driver_rating}}" name="rating" class="rating"/>
                                </div>
                            </td>
                            <td><span class="text-muted">{{$review->created_at->diffForHumans()}}</span></td>
                            <!--<td>{{$review->provider_comment}}</td>-->
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>@lang('admin.id')</th>
                            <th>@lang('admin.request.Driver_Name')</th>
                            <th>@lang('admin.request.User_Name')</th>
                            <th>@lang('admin.review.Rating')</th>
                            <th>@lang('admin.request.Date_Time')</th>
                            <!--<th>@lang('admin.review.Comments')</th> -->
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection