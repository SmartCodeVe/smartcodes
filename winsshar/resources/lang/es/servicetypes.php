<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Service Types Translations
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'MIN'           => 'Precios por Minuto',
    'HOUR'          => 'Precios por Hora',
    'DISTANCE'      => 'Precios Distancia',
    'DISTANCEMIN'   => 'Distancia y Precio por Minuto',
    'DISTANCEHOUR'  => 'Distancia y Precio por Hora',

];
