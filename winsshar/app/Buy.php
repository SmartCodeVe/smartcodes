<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buy extends Model
{
    protected $fillable = [
        'num_orden', 'user_bussiness_id', 'amount', 'pending', 'created', 'status', 'title', 'currency'
    ];
}
