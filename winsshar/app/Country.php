<?php

namespace App;


use Illuminate\Notifications\Notifiable;


class Country
{
    use Notifiable;

    public $timestamps = false;
}
