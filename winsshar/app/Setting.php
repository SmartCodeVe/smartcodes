<?php

namespace App;


use Illuminate\Notifications\Notifiable;


class Setting
{
    use Notifiable;

    public $timestamps = false;
}
