<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;


class LevelComission extends Model
{
    use Notifiable;

    protected $table = "level_comissions";
}
