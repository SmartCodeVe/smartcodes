<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use App\RiderRequest;

class DriverDocument extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver_id', 'document_id','url'
    ];


    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }

    public function document()
    {
        return $this->belongsTo('App\Document');
    }
    
    
}
