<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'stripe_token', 'card_number', 'card_holder_name', 'card_exp_date', 'card_cvc','user_id'
    ];
}
