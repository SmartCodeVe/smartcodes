<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;


class UserOnlineBussiness extends Model
{
    use Notifiable;

    protected $table = 'user_online_bussiness';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'link', 'picture', 'online_provider_id'
    ];
}
