<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Notifications\Notifiable;
use App\RiderRequest;

class Driver extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'latitude', 'longitude','proof_status','online_status','license','insurance','user_bussiness_id'
    ];


    public function userBussiness()
    {
        return $this->belongsTo('App\UserBussiness');
    }


    public function acceptedRequest()
    {
        return RiderRequest::where('driver_id',$this->id)->where('request_status','accepted')->get()->count();
    }


    public function cancelledRequest()
    {
        return RiderRequest::where('driver_id',$this->id)->where('request_status','cancel')->get()->count();
    }
    
    
}
