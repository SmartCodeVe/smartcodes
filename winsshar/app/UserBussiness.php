<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\BussinessType;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBussiness extends Model
{
    use Notifiable,SoftDeletes;

    protected $table = 'user_bussiness';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'website', 'bussiness_type_id', 'latitude', 'longitude','approved','user_id','active'
    ];

    
    public function user() {
        return $this->belongsTo(User::Class,'user_id','id');
    }

    public function bussinesType() {
        return $this->belongsTo(BussinessType::Class,'bussiness_type_id','id');
    }

    

}
