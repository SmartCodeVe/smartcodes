<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class UpdateRequest extends Request {

  public function rules() {
   
    $rules = [
      "request_id" => "required",
      "driver_id" => "required",
      "request_status" => "required",
      "lat" => "required",
      "long" => "required",
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}