<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class ProfileRequest extends Request {

  public function rules() {
   
    $rules = [
      "name" => "required",
      "dni" => "required",
      "mobile" => "required",
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}