<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class UpdateLocationRequest extends Request {

  public function rules() {
   
    $rules = [
      "driverid" => "required",
      "latitude" => "required",
      "longitude" => "required"
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}