<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class SignupRequest extends Request {

  public function rules() {
   
    $rules = [
      "name" => "required",
      "email" => "required",
      "mobile" => "required",
      "password" => "required",
    ];
    
    return $rules;
  }


  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}