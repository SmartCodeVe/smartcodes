<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class PaymentSendRequest extends Request {

  public function rules() {
   
    $rules = [
      "user_bussiness_id" => "required",
      "amount" => "required"
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}