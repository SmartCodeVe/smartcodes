<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\JsonApi\Error;
use App\JsonApi\JsonApiFormater;
use Illuminate\Http\JsonResponse;

abstract class Request extends FormRequest
{


  public function response(array $errors) {
    $jsonApi = new JsonApiFormater([]);
    foreach ($errors as $field=> $badfield) {
      foreach ($badfield as $mens) {
        $error = new Error('field-error',$mens,$field);
        $jsonApi->addError($error);
      }
    }
    $jsonApi->setCode("Fail");
    return new JsonResponse($jsonApi->asError(), 400);
  }

  public function ajax() {
    return TRUE;
  }

  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize() {
    return TRUE;
  }

}
