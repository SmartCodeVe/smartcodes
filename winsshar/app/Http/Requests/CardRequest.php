<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class CardRequest extends Request {

  public function rules() {
   
    $rules = [
      "stripe_token" => "required",
      "card_number" => "required",
      "card_holder_name" => "required",
      "card_exp_date" => "required",
      "card_cvc" => "required"
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}