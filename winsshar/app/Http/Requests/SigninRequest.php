<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class SigninRequest extends Request {

  public function rules() {
   
    $rules = [
      "email" => "required",
      "password" => "required"
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}