<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class ImageUploadRequest extends Request {

  public function rules() {
   
    $rules = [
      "uploadedfile" => "required",
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}