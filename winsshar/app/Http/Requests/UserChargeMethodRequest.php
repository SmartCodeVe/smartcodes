<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class UserChargeMethodRequest extends Request {

  public function rules() {
   
    $rules = [
      "account_name" => "required",
      "account_number" => "required",
      "swift_code" => "required"
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}