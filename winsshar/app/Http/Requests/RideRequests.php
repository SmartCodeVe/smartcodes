<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class RideRequests extends Request {

  public function rules() {
   
    $rules = [
      "start_lat" => "required",
      "start_long" => "required",
      "end_lat" => "required",
      "end_long" => "required",
      "pickup_address" => "required",
      "drop_address" => "required"
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}