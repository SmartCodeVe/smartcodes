<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class UserBussinessRequest extends Request {

  public function rules() {
   
    $rules = [
      "name" => "required",
      "bussiness_type_id" => "required"
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}