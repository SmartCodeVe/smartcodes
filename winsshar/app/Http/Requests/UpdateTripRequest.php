<?php

namespace App\Http\Requests;
use App\Http\Requests\Request;

class UpdateTripRequest extends Request {

  public function rules() {
   
    $rules = [
      "trip_id" => "required",
      "trip_status" => "required",
      "accept_status" => "required",
      "distance" => "required",
      "total_amount"=> "required"
    ];
    
    return $rules;
  }

  public function messages() {
    return [];
  }

  public function ajax() {
    return TRUE;
  }

  public function authorize() {
    return TRUE;
  }

}