<?php
namespace App\Http\Controllers\Api;

if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

use Stripe\Charge;
use Stripe\Stripe;
use Stripe\StripeInvalidRequestError;

use DB;
use Log;
use Auth;
use Hash;
use Storage;
use Setting;
use Exception;
use Notification;

use Carbon\Carbon;
use App\Helpers\Helper;

use App\Http\Requests\ImageUploadRequest;
use App\Http\Requests\BussinessByTypeRequest;
use App\Http\Requests\PaymentSendRequest;
use App\Http\Requests\UpdateLocationRequest;
use App\Http\Requests\RideRequests;
use App\Http\Requests\UpdateRequest;
use App\Http\Requests\UpdateTripRequest;



use App\User;
use App\Rate;
use App\Referral;
use App\OnlineProvider;
use App\UserOnlineBussiness;
use App\UserBussiness;
use App\Earning;
use App\RiderRequest;
use App\Trip;
use App\Driver;
use App\LevelComission;
use App\NetworkEarning;
use App\UserCard;
use App\Document;


use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class GeneralController extends Controller
{

    public function getRates(Request $request){
        $response = Rate::all()->first();
        return response()->json($response, 200);
    }

    public function imageUpload(ImageUploadRequest $request)
    {   
        $postData = $request->all();
        
        if ($request->hasFile('uploadedfile')) {
            $image = $request->file('uploadedfile');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $name);
            
            $reponse['status'] = "Success";
            $response['image_name'] = $name;
            $response['imageurl'] = $name;
        }else{
            $response['status'] = "Fail";
        }
        return response()->json(array($response), 200);
    }

    public function getDocuments(Request $request){
        $response = Document::all();
        return response()->json($response, 200);
    }

    private function getReferralsByOwner($owner){
        $referrals = DB::table('referrals')
        ->join('users','referrals.user_id','=','users.id')
        ->select(DB::raw('users.id, users.name, users.email, users.dni, users.picture,users.pixel, users.mobile'))
        ->where('owner_id',$owner)->get();
        return $referrals->count() > 0 ? $referrals : [];
    }

    public function getReferrals(Request $request,$level,$user_id){
        $getData = $request->all();
        if($level!='' && $user_id!=''){
            if($level>1){
                $index = 1;
                $owner = array($user_id);
                while($index <= $level){
                    $referrals = $this->getReferralsByOwner($owner);
                    if($referrals!=null){
                        $owner = [];
                        foreach($referrals as $r){
                            array_push($owner, $r->id);
                        }
                    }
                    $index++; 
                }
                $result = $referrals;
            }else{
                $result = $this->getReferralsByOwner(array($user_id));
            }

            $response = $result;
        }else{
            $response = array();
        }
        return response()->json($response, 200);
    }

    public function getBussinessByType(BussinessByTypeRequest $request){
        $postData = $request->all();
        $results = DB::select(DB::raw("SELECT ub.id,ub.address,bt.icon,ub.name,ub.latitude,ub.longitude,3956 * 2 * ASIN(SQRT(POWER(SIN((:latitude - ub.latitude) * pi()/180 / 2), 2)
        + COS(:latitude_2 * pi()/180 ) * COS(:latitude_3 * pi()/180)
        * POWER(SIN((:longitude - ub.longitude) * pi()/180 / 2), 2) )) as distance
        FROM user_bussiness ub, bussiness_types bt
        WHERE ub.bussiness_type_id = :bt AND ub.active = 1 and bt.id = ub.bussiness_type_id HAVING distance < :distance 
        ORDER BY distance ASC"), array( 
            'latitude' => floatval($postData["latitude"]),
            'latitude_2' => floatval($postData["latitude"]),
            'latitude_3' => floatval($postData["latitude"]),
            'longitude' => floatval($postData["longitude"]),
            'bt' => floatval($postData["bussiness_type"]),
            'distance'=> 1000
        ));
        return response()->json($results, 200);
    }

    public function getOnlineProviders(Request $request){
        $response = OnlineProvider::where('active',true)->get();
        return response()->json($response, 200);
    }

    public function getProviderItems(Request $request,$id){
        $response = UserOnlineBussiness::where('online_provider_id',$id)->get();
        return response()->json($response, 200);
    }

    public function getAllUserBussiness(Request $request){
        $response = UserBussiness::with('user')->where('active',true)->get();
        return response()->json($response, 200);
    }

    public function savePaymentSend(PaymentSendRequest $request){
        $postData = $request->all();
        try{
            $customer = Auth::user();
            $userBussiness = UserBussiness::find($postData["user_bussiness_id"]);

            $total = number_format($postData['amount'], 2, '.', '');

            $postData['customer_id'] = $customer->id;
            $postData['comission'] = "0.8";
            $postData['total_amount'] = $postData['amount'] - 0.8;
            Earning::create($postData);

            //stripe charge
            try{
                Stripe::setApiKey("sk_test_0SGgDUVGsXLzbkyG2tjPQrHq");
                Charge::create([
                    'customer' => $customer->stripe_customer_id,
                    'description' => "Pago realizado a " . $userBussiness->name,
                    'amount' => str_replace(".","",$total),
                    'currency' => 'usd',
                    'receipt_email' => $customer->email
                ]);
            }catch (\Stripe\Error\Card $e) {
                
            }
            //network
            $network = $this->get_network_level($userBussiness->user_id);
            if(count($network)>0){
                foreach($network as $key=>$n){
                    $amount = bcdiv(($n*0.8)/100, 1, 2);
                    $data_network['referral_id'] = $userBussiness->user_id;
                    $data_network['user_id'] = $key;
                    $data_network['amount'] = $amount;
                    NetworkEarning::create($data_network);
                    //update network balance
                    $u = User::find($key);
                    $u->network_balance = $u->network_balance + $amount;
                    $u->save();
                }
            }

            //update wallet balance
            $user = User::find($userBussiness->user_id);
            $user->earning_balance = $user->earning_balance +  ($postData['amount'] - 0.8);
            $user->save();

            $response['code']="Success";
            return response()->json($response, 200);
		}catch (Exception $e) {
           return response()->json(['code'=>'Fail','message'=>'No se pudo registrar el pago'], 500);
        }
    }

    public function updateLocation(UpdateLocationRequest $request){
        $postData = $request->all();
        try{
            $driver = Driver::find($postData['driverid']);
            $driver->latitude = $postData['latitude'];
            $driver->longitude = $postData['longitude'];
            $driver->save();
            return response()->json(['code'=>'Success'], 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail'], 500);
        }
    }

    public function setRequest(RideRequests $request){
        $postData = $request->all();
        try{
            $data['pickup_lat'] = $postData['start_lat'];	
			$data['pickup_long'] = $postData['start_long'];	
			$data['destination_lat'] = $postData['end_lat'];	
			$data['destination_long'] = $postData['end_long'];	
            $data["request_status"] = "processing";
            $data["user_id"] = Auth::user()->id;
            $data['pickup_address'] = urldecode(utf8_encode($postData['pickup_address']));	
			$data['drop_address'] = urldecode(utf8_decode($postData['drop_address']));	
            $response = RiderRequest::create($data);
            $response['code'] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail'], 500);
        }
        
    }

    public function processRequest(Request $request,$request_id){
        $serviceAccount = ServiceAccount::fromJsonFile(base_path().'/firebase_auth.json');
        $firebase = (new Factory)->withServiceAccount($serviceAccount)->withDatabaseUri('https://winsshare-peru.firebaseio.com')->create();
        
        $getRequestData = RiderRequest::find($request_id);
        
        $lat = $getRequestData->pickup_lat;
        $long = $getRequestData->pickup_long;

        $maxDistance = 1000;
        $request_duration_time = 15;

        $cursor = DB::select(DB::raw("SELECT *,3956 * 2 * ASIN(SQRT(POWER(SIN((:latitude - latitude) * pi()/180 / 2), 2)
        + COS(:latitude_2 * pi()/180 ) * COS(latitude * pi()/180)
        * POWER(SIN((:longitude - drivers.longitude) * pi()/180 / 2), 2) )) as distance
        FROM drivers 
        WHERE online_status = 1 HAVING distance < :distance
        ORDER BY distance ASC LIMIT 0,1"), array( 
            'latitude' => floatval($lat),
            'latitude_2' => floatval($lat),
            'longitude' => floatval($long),
            'distance'=> 10000));
        
        $count_drivers = count($cursor);
        $i = 0 ;
        if($getRequestData->request_status != 'cancel'){	
            if($count_drivers >=1){	
                $database = $firebase->getDatabase();
                foreach ($cursor as $doc){

                    if($getRequestData->request_status != 'cancel') {
                        $request_status = $database->getReference('drivers_data/'.$doc->id.'/request')->getValue(); 
                        $accept_status = $database->getReference('drivers_data/'.$doc->id.'/accept')->getValue(); 
                        if(($request_status['status'] == 0 || $request_status['status'] == "")  &&  ($accept_status['status'] == 0 ||  $accept_status['status'] == '')){
                            $get_dist = $this->GetDrivingDistance($doc->latitude,$doc->longitude,$getRequestData->pickup_lat ,$getRequestData->pickup_long);
                            $rider = Auth::user();
                            $r=$database
                            ->getReference('drivers_data/'.$doc->id.'/request')
                            ->set([
                                'status' => 1,
                                'eta' => $get_dist['time'],
                                'req_id' => $request_id,
                                'rider_id' => $getRequestData->user_id,
                                'rider_name' => $rider->name,
                                'rider_pic' => $rider->picture,
                                'origin_addr' => $getRequestData->pickup_address,
                                'dest_addr' => $getRequestData->drop_address
                            ]);
                        }
                        sleep($request_duration_time);
                        $request_status = $database->getReference('drivers_data/'.$doc->id.'/request')->getValue(); 
                        $accept_status = $database->getReference('drivers_data/'.$doc->id.'/accept')->getValue(); 
                        
                            if( $request_status['status'] == 1 
									&&  $request_status['req_id'] == $request_id 
										&&  $accept_status['status'] == 1 ) // Check driver accepted or not
								{
                                    $data_trip['pickup_lat'] =  $getRequestData->pickup_lat;
									$data_trip['pickup_long'] =  $getRequestData->pickup_long ;
									$data_trip['destination_lat'] = $getRequestData->destination_lat;
									$data_trip['destination_long'] = $getRequestData->destination_long;
									$data_trip['pickup_address'] = $getRequestData->pickup_address;
									$data_trip['drop_address'] = $getRequestData->drop_address;
									
									$data_trip['user_id'] = $getRequestData->user_id;	
									$data_trip['accept_status'] = "1"; // Accepted
									$data_trip['trip_status'] = "off";
									$data_trip['driver_id'] = $doc->id;
									$data_trip['total_price'] = "0";
									$data_trip['driver_location_lat'] =  $doc->latitude;
									$data_trip['driver_location_long'] =  $doc->longitude;
									
									$data_trip['request_id'] = $request_id;
                                    $trip = Trip::create($data_trip);
                                    
                                    $getRequestData->trip_id = $trip->id;
                                    $getRequestData->request_status = "accepted";
                                    $getRequestData->save();

                                    $r=$database
                                    ->getReference('drivers_data/'.$doc->id.'/accept')
                                    ->set(array('trip_id' => $trip->id));

									$response = $trip->toArray();
									break;
                                }else{
                                    $r=$database
                                    ->getReference('drivers_data/'.$doc->id.'/request')
                                    ->set([
										'status' => 0,
										'eta' => 0,
										'req_id' => '',
										'rider_id' => '',
										'rider_name' => '',
										'rider_pic' => '',
										'origin_addr' => '',
										'dest_addr' => ''
                                    ]);

									if ($i == $count_drivers - 1){
										$getRequestData->request_status = "no_driver";
                                        $getRequestData->save();
									}
                                    $getRequestData = RiderRequest::find($request_id);
                                    $response = $getRequestData->toArray();
                                }
                            
                    }else{
                        $getRequestData = RiderRequest::find($request_id);
						$response = $getRequestData->toArray();
						break ;
                    }
                    $i++;
                }
            }else{
                $getRequestData->request_status = "no_driver";
                $getRequestData->save();
                $response = $getRequestData->toArray();
            }
        }

        return response()->json($response, 200);
    }

    public function getRiderProfile(Request $request,$user_id)
    {
        try{
            $response = User::find($user_id);
            $response['code'] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail'], 500);
        }
    }

    public function getDriverProfile(Request $request,$driver_id)
    {
        try{
            $driver = Driver::find($driver_id);
            $userBussiness = UserBussiness::find($driver->user_bussiness_id);
            $user = User::find($userBussiness->user_id);
            $response = $user->toArray();
            $response['code'] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail'], 500);
        }
    }

    
    public function updateRequest(UpdateRequest $request)
    {
        $postData = $request->all();
        try{
            $riderRequest = RiderRequest::find($postData["request_id"]);
            $riderRequest->request_status = $postData["request_status"];
            $riderRequest->driver_id = $postData["driver_id"];
            $riderRequest->driver_location_lat = $postData["lat"];
            $riderRequest->driver_location_long = $postData["long"];
            $riderRequest->save();
            $response = $riderRequest->toArray();
            $response["code"] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail'], 500);
        }
    }

    public function updateOnlineStatus(Request $request,$driverid,$status){
        try{
            $driver = Driver::find($driverid);
            $driver->online_status = $status;
            $driver->save();
            $response["code"] = "Success";
            $response["online_status"] = $status == '1' ? 'online' : 'offline';
            $response["data"] = $response["online_status"];
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail'], 500);
        }
    }

    public function updateTrips(UpdateTripRequest $request){
        $postData = $request->all();
        try{
            $trip = Trip::find($postData['trip_id']);

            $serviceAccount = ServiceAccount::fromJsonFile(base_path().'/firebase_auth.json');
            $firebase = (new Factory)->withServiceAccount($serviceAccount)->withDatabaseUri('https://winsshare-peru.firebaseio.com')->create();

            $trip->total_price = $postData['total_amount'];
            $trip->total_distance = $postData['distance'];
            $trip->payment_status ='Not completed';
            $trip->trip_status = $postData['trip_status'];
            $trip->accept_status = $postData['accept_status'];

            if($postData['trip_status']=='end'){
                $driver = Driver::find($trip->driver_id);
                $total_amount = $postData['total_amount'] > 0 ? $postData['total_amount'] : 0;
                $total_price = number_format($total_amount + 0.8, 2, '.', '') ;

                $data_earning['user_bussiness_id'] = $driver->user_bussiness_id;
                $data_earning['amount'] = $total_price;
                $data_earning['comission'] = 0.8;
                $data_earning['total_amount'] = $total_amount;
                $data_earning['customer_id'] = $trip->user_id;
                Earning::create($data_earning);

                $userBussiness = UserBussiness::find($driver->user_bussiness_id);
                $customer = User::find($trip->user_id);
				$card = UserCard::where("user_id",$customer->id)->first();
                //stripe charge
                try{
                    Stripe::setApiKey("sk_test_0SGgDUVGsXLzbkyG2tjPQrHq");
                    Charge::create([
                        'customer' => $customer->stripe_customer_id,
                        'description' => "Servicio de Taxi",
                        'amount' => str_replace(".","",$total_price),
                        'currency' => 'usd',
                        'receipt_email' => $customer->email
                    ]);
                }catch (\Stripe\Error\Card $e) {
                    
                }
                //network
                $network = $this->get_network_level($userBussiness->user_id);
                if(count($network)>0){
                    foreach($network as $key=>$n){
                        $amount = bcdiv(($n*0.8)/100, 1, 2);
                        $data_network['referral_id'] = $userBussiness->user_id;
                        $data_network['user_id'] = $key;
                        $data_network['amount'] = $amount;
                        NetworkEarning::create($data_network);
                        //update network balance
                        $u = User::find($key);
                        $u->network_balance = $u->network_balance + $amount;
                        $u->save();
                    }
                }

                //update wallet balance
                $user = User::find($userBussiness->user_id);
                $user->earning_balance = $user->earning_balance +  $total_amount;
                $user->save();

            }
            $trip->save();
            $response = $trip->toArray();
            $response["code"] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail'], 500);
        }

    }

    public function getRequest(Request $request,$request_id){
        try{
            $r = RiderRequest::find($request_id);
            $response = $r->toArray();
            $response["code"] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail'], 500);
        }
    }

    private function GetDrivingDistance($lat1, $long1, $lat2, $long2)
    {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&key=";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);

        curl_close($ch);
        $response_a = json_decode($response, true);
        if(count($response_a['rows'])>0){
            $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
            $time = $response_a['rows'][0]['elements'][0]['duration']['text'];
        }else{
            $dist = "";
            $time = "";
        }

        return array('distance' => $dist, 'time' => $time);
    }

    private function get_network_level($id){
        $levels = [];
        $found = true;
        $user = $id;
        $network = array();
        while($found){
            $records = Referral::where('user_id',$user)->get();
            if(count($records) > 0){
                $row = $records->first();
                $user = $row->owner_id;
                $levels[] = $user;
            }else{
                $found = false;
            }
        }
        if(count($levels)>0){
            $levels = array_reverse($levels);
            $comissions = DB::table('level_comissions')->limit(count($levels))->get();
            foreach($levels as $key => $l){
                $network[$l] = $comissions[$key]->comission;
            }
        }
        return $network;
    }

}