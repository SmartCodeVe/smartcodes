<?php

namespace App\Http\Controllers\Api;

if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;
use Log;
use Auth;
use Hash;
use Storage;
use Setting;
use Exception;
use Notification;

use Carbon\Carbon;
use App\Helpers\Helper;
use Stripe\Stripe;
use Stripe\Customer;

use App\Http\Requests\EmailPhoneRequest;
use App\Http\Requests\SignupRequest;
use App\Http\Requests\SigninRequest;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\CardRequest;
use App\Http\Requests\UserChargeMethodRequest;

use App\User;
use App\Referral;
use App\UserBussiness;
use App\Driver;
use App\UserCard;
use App\UserChargeMethod;
use App\Earning;
use App\Withdrawal;


use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{

    public function checkEmailPhone(EmailPhoneRequest $request){
        $postData = $request->all();
        $email_result = User::where('email',$postData['email'])->count();
        $mobile_result = User::where('mobile',$postData['mobile'])->count();

        if($email_result > 0 && $mobile_result > 0){
            $response['code'] = "Fails";
            $response['message'] = "both_exists";
        }else if($email_result > 0){
            $response['code'] = "Fails";
            $response['message'] = "email_exists";
        }else if($mobile_result > 0){
            $response['code'] = "Fails";
            $response['message'] = "mobile_exists";
        }else{
            $response['code'] = "Success";
        }

        return response()->json($response, 200);
    }

    public function signup(SignupRequest $request)
    {
        $postData = $request->all();
        try{
            $postData["password"] = bcrypt($postData["password"]);
            $postData["pixel"] = mt_rand(0,99999999);
            $postData["name"] = str_replace("%20"," ",$postData["name"]);
            $user = User::create($postData);

            if(array_key_exists("referral_code",$postData)){
                $referral_code = $postData["referral_code"];
                if($referral_code!='-1'){
                    $owner_user = User::where('pixel',$referral_code)->first();
                    $referral = new Referral();
                    $referral->owner_id = $owner_user->id;
                    $referral->user_id = $user->id;
                    $referral->save();
                }
            }

            $response = $user->toArray();
            $response['code'] = "Success";
            $response['auth_token'] = JWTAuth::fromUser($user);
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail','error' => 'insert_error'], 500);
        }
    }

    public function signin(SigninRequest $request){
        $postData = $request->all();
        try {
            if (!$token = JWTAuth::attempt($postData)) {
                return response()->json(['code'=>'Fail','error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['code'=>'Fail','error' => 'could_not_create_token'], 500);
        }
        $response = User::where("email",$postData["email"])->first()->toArray();
        $response['code'] = "Success";
        $response['auth_token'] = compact('token')['token'];
        $user_bussiness = UserBussiness::where('user_id',Auth::user()->id)->pluck('id')->toArray();
        if(count($user_bussiness)>0){
            $driver = Driver::whereIn('user_bussiness_id',$user_bussiness);
            if($driver->count()>0){
                $response['driver'] = $driver->first();
            }
        }

        return response()->json($response, 200);
    }

    public function updateFcmToken(Request $request){
        $postData = $request->all();
        if(array_key_exists('token',$postData)){
            $user = Auth::user();
            $user->fcm_token = $postData["token"];
            $user->save();
            $response['code'] = "Success";
        }else{
            $response['code'] = "Fail";
        }
        return response()->json($response, 200);
    }

    public function getAvailableEarnings(Request $request){
        $response = User::find(Auth::user()->id)->toArray();
        return response()->json($response, 200);
    }

    public function getResumeData(Request $request){
        $amount = DB::table('network_earnings')
        ->where('referral_id',Auth::user()->id)
        ->sum('amount');
        $response['total_earnings'] = $amount != null ? $amount : "0.00";
        $response['partners'] = $this->countNetwork();
        $response['code']="Success";
        return response()->json($response, 200);
    }

    public function getUserEarnings(Request $request){
        $amount = DB::table('earnings')
        ->join('user_bussiness','user_bussiness.id','=','earnings.user_bussiness_id')
        ->where('user_bussiness.user_id',Auth::user()->id)
        ->sum('total_amount');

        $response['code']="Success";
        $response['data'] = $amount != null ? $amount : "0.00";
        return response()->json($response, 200);
    }

    public function updateUser(ProfileRequest $request){
        $postData = $request->all();
        try{
            $postData['name'] = str_replace("%20"," ",$postData["name"]);
            $user = User::find(Auth::user()->id);
            $user->update($postData);
            return response()->json(['code'=>'Success'], 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail','error' => $e->getMessage()], 500);
        }
        
    }

    public function getNetworkEarnings(Request $request){
        $amount = DB::table('network_earnings')
        ->where('referral_id',Auth::user()->id)
        ->sum('amount');
        $response['code']="Success";
        $response['data'] = $amount != null ? $amount : "0.00";
        return response()->json($response, 200);
    }

    public function userCard(Request $request){
        $result = UserCard::where('user_id',Auth::user()->id);
        if($result->count()>0){
            $card = $result->first();
            $response['code']="Success";
		  	$response['data']= $card->card_number;
		}else{
			$response['code']="Fails";
		}
		return response()->json($response, 200);
    }
    
    public function saveCard(CardRequest $request){
        $postData = $request->all();
        try{
            $user =  Auth::user();
            $postData["user_id"] = $user->id;
            $postData["card_number"] = substr($postData["card_number"], -4);
            UserCard::where('user_id',Auth::user()->id)->delete();
            UserCard::create($postData);
            try{
                
                Stripe::setApiKey("sk_test_0SGgDUVGsXLzbkyG2tjPQrHq");
                $customer = Customer::create([
                    'source' => $postData["stripe_token"],
                    'description' => $user->name
                ]);
                $user->stripe_customer_id = $customer->id;
                $user->save();
            }catch  (Exception $e) {

            }

            $response['code']="Success";
            return response()->json($response, 200);
		}catch (Exception $e) {
           return response()->json(['code'=>'Fail','message'=>'No se pudo guardar los datos de la tarjeta'], 500);
        }
    }
    
    public function userChargeMethod(Request $request){
        $result = UserChargeMethod::where('user_id',Auth::user()->id);
        if($result->count()>0){
            $charge = $result->first();
            $response['code']="Success";
		  	$response['data']= $charge->account_number;
		}else{
			$response['code']="Fails";
		}
		return response()->json($response, 200);
    }

    public function saveChargeMethod(userChargeMethodRequest $request){
        $postData = $request->all();
        try{
            $postData["user_id"] = Auth::user()->id;
            UserChargeMethod::where('user_id',Auth::user()->id)->delete();
            UserChargeMethod::create($postData);
            $response['code']="Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
           return response()->json(['code'=>'Fail','message'=>'No se pudo guardar los datos de la cuenta'], 500);
        }
	}

    public function userBussiness(){
        $response = DB::table('user_bussiness')
        ->where('user_bussiness.user_id',Auth::user()->id)
        ->get();
		return response()->json($response, 200);
    }

    public function getEarningsHistory(Request $request)
	{
        $earnings = DB::table('earnings')
        ->join('user_bussiness','user_bussiness.id','=','earnings.user_bussiness_id')
        ->where('user_bussiness.user_id',Auth::user()->id)
        ->select(DB::raw('earnings.*, user_bussiness.name as bussinessName'))
        ->get();
        return response()->json($earnings, 200);
	}

	public function getUserWithdrawals(Request $request)
	{
        $withdrawals = DB::table('withdrawals')
        ->where('withdrawals.user_id',Auth::user()->id)
        ->get();
        return response()->json($withdrawals, 200);
    }
    
    public function getMyPayments(Request $request){
        $response = Earning::with('userBussiness')->where('customer_id',Auth::user()->id)->get();
        return response()->json($response, 200);
    }

    public function getUserActivity(Request $request){
        $response['user_bussiness'] = DB::table('user_bussiness')
        ->where('user_bussiness.user_id',Auth::user()->id)
        ->get();
        $response['earning_list'] = DB::table('earnings')
        ->join('user_bussiness','user_bussiness.id','=','earnings.user_bussiness_id')
        ->where('user_bussiness.user_id',Auth::user()->id)
        ->select(DB::raw('earnings.*, user_bussiness.name as bussinessName'))
        ->get();
        $response['withdrawal_list'] = DB::table('withdrawals')
        ->where('withdrawals.user_id',Auth::user()->id)
        ->get();
        $response['payment_list'] = Earning::with('userBussiness')->where('customer_id',Auth::user()->id)->get();
        $response['code'] = "Success";
        return response()->json($response, 200);
    }

    public function getWalletBalance(Request $request){
        $user = Auth::user();
        $response['wallet_balance'] = ($user->wallet_balance!=null ? $user->wallet_balance : "0.00");
        $response['bitcoin_balance'] = ($user->bitcoin_balance!=null ? $user->bitcoin_balance : "0.00");
        $response['winsshar_balance'] = ($user->winsshar_balance!=null ? $user->winsshar_balance : "0.00");
        return response()->json($response, 200);
    }

    public function addWalletBalance(Request $request){
        $postData = $request->all();
        if(array_key_exists("amount",$postData) && $postData['amount']!=""){
            $user = Auth::user();
            try{
                $user->wallet_balance = $user->wallet_balance + $postData['amount'];
                $user->save();
                $response['code'] = "Success";
                $response['data'] = $user->wallet_balance;
                return response()->json($response, 200);
            }catch (Exception $e) {
                return response()->json(['code'=>'Fail','message'=>'No se pudo completa la transaccion'], 500);
            }
        }else{
            return response()->json(['code'=>'Fail','message'=>'No se agrego dinero a la cartera'], 500);
        }
    }

    public function networkWithdrawal(Request $request){
        try{
            $user = Auth::user();
            $data['user_id'] = $user->id;
            $data['amount'] =  $user->network_balance;
            $data['status'] = 'Pending';
            Withdrawal::create($data);
            $user->network_balance = 0;
            $user->save();
            $response['code'] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail','message'=>'No se pudo completa la transaccion'], 500);
        }
        return response()->json($response, 200);
    }

    public function bussinessWithdrawal(Request $request){
        try{
            $user = Auth::user();
            $data['user_id'] = $user->id;
            $data['amount'] =  $user->earning_balance;
            $data['status'] = 'Pending';
            Withdrawal::create($data);
            $user->earning_balance = 0;
            $user->save();
            $response['code'] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail','message'=>'No se pudo completa la transaccion'], 500);
        }
        return response()->json($response, 200);
    }
    
    private function countNetwork(){
        $found = true;
        $user = array(Auth::user()->id);
        $total_count = 0;
        while($found){
            $records = Referral::whereIn('owner_id',$user)->limit(10)->get();
            if(count($records) > 0){
                $user = [];
                $total_count += count($records);
                foreach($records as $r){
                    $user[] = $r->user_id;
                }
            }else{
                $found = false;
            }
        }
        return $total_count;
    }
}