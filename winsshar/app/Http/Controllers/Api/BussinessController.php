<?php

namespace App\Http\Controllers\Api;

if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use DB;
use Log;
use Auth;
use Hash;
use Storage;
use Setting;
use Exception;
use Notification;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

use Carbon\Carbon;
use App\Helpers\Helper;

use App\Http\Requests\UserBussinessRequest;

use App\User;
use App\BussinessType;
use App\UserBussiness;
use App\Driver;
use App\Document;
use App\DriverDocument;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class BussinessController extends Controller
{

    public function getBussinessTypes(Request $request){
        $response = BussinessType::all();
        return response()->json($response, 200);
    }

    public function addUserBussiness(UserBussinessRequest $request){
        $postData = $request->all();
        try{
            $postData["active"] = true;
            if(array_key_exists("not_approved",$postData)){
                $postData["approved"] = false;
            }else{
                $postData["approved"] = true;
            }
            $postData["user_id"] =Auth::user()->id;
            $userBussiness = UserBussiness::create($postData);

            if(array_key_exists("document_ids",$postData)){
                $documents = Document::all();
                $tmp_ids = explode(",",$postData["document_ids"]);
                $tmp_urls = explode(",",$postData["document_urls"]);
                if(count($tmp_ids)>0){
                    $driverData = array();
                    $driverData['proof_status'] = 'Accepted';
                    $driverData['online_status'] = 0;
                    $driverData['user_bussiness_id'] =  $userBussiness->id;
                    $driver = Driver::create($driverData);
                    foreach($tmp_ids as $key=>$doc){
                        $docData = array();
                        $docData['driver_id'] = $driver->id;
                        $docData['document_id'] = $doc;
                        $docData['url'] = $tmp_urls[$key];
                        DriverDocument::create($docData);
                    }
                    $response = $driver->toArray();
                    $response['userBussiness'] = $userBussiness->toArray();
                }
            }
            $response["data"] = $postData;
            $response["code"] = "Success";
            return response()->json($response, 200);
        }catch (Exception $e) {
            return response()->json(['code'=>'Fail','error' => $e->getMessage()], 500);
        }
    }

    
}