<?php

namespace App\Http\Controllers;
use App\Sale;

use Illuminate\Http\Request;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Instanciamos la clase
        $sale = new Sale;

        //Declaramos el nombre con el nombre enviado en el request
        $sale->num_orden = 1;
        $sale->user_bussiness_id = $request->user_bussiness_id;
        $sale->title = $request->title;
        $sale->amount = $request->amount;
        $sale->title = $request->title;
        $sale->pending = $request->amount;
        $sale->currency = $request->currency;
        $sale->status = "0";

        //Guardamos el cambio en nuestro modelo
        $sale->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_bussiness_id)
    {
        return Sale::where('user_bussiness_id', $user_bussiness_id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showBuyByUserId($id)
    {
        return Sale::where('user_bussiness_id', $id)->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
