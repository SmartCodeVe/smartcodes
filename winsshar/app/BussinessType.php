<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Notifications\Notifiable;


class BussinessType extends Model
{
    use Notifiable,SoftDeletes;

    protected $table = 'bussiness_types';

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'active', 'icon'];
    
}
