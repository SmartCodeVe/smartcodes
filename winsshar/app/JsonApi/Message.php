<?php

class Message {

   /**
    * @var codigo o palabra relacionada al resultado de la operación "sucess", "error", "warning"
    */
    public $code;

   /**
    * @var Mensaje a mostrar al usuario
    */
    public $info;
}