<?php

namespace App\JsonApi;
class Error
{
    /**
     * @var string o palabra relacionada con el error
     */

    public $code;

    /**
     * @var  string del dato que arroja el error
     */
    public $data;

    /**
     * @var string en el campo nombre
     */
    public $message;

    /**
     * Error constructor.
     * @param string $code
     * @param string $message
     * @param string $data
     */
    public function __construct($code, $message, $data) {
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
    }


}