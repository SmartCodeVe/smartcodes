<?php

namespace App\JsonApi;

/**
 *
 * Esta clase permite formatear una respuesta
 *
 */
class JsonApiFormater {

    public $data= array();
    public $errors = array();
    public $message;
    public $code;

    public function __construct($data=[]){
        try{
            $this->data = $data;
        } catch (Exception $ex) {
            $this->data[] = $data;
        }
    }

    public function addError(Error $error, $field = NULL) {
        $this->errors[] = $error;
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function addData($data) {
        $this->data = $data;
    }

    public function setMessage($message) {
        $this->message = $message;
    }

    public function asData() {
        unset($this->errors);
        return $this;
    }

    public function asError() {
        unset($this->data);
        return $this;
    }

    public function setCode($code) {
        $this->code = $code;
    }


}