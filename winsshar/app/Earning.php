<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Earning extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_bussiness_id', 'amount','comission', 'total_amount','customer_id'
    ];

    public function userBussiness()
    {
        return $this->belongsTo('App\UserBussiness');
    }

    public function customer()
    {
        return $this->belongsTo('App\User','customer_id');
    }
}
