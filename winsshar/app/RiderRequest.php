<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class RiderRequest extends Model
{

    use Notifiable;

    protected $table="requests";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pickup_lat', 'pickup_long','destination_lat', 'destination_long','user_id','pickup_address','drop_address','trip_id','request_status','driver_id','eta','driver_location_lat','driver_location_long'
    ];

    /**
     * The user who created the request.
    */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * The driver who accept the request.
    */
    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }

}
