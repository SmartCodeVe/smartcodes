<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pickup_lat', 'pickup_long','destination_lat', 'destination_long','user_id','pickup_address','drop_address','accept_status','trip_status','driver_id','total_price','total_distance','driver_location_lat','driver_location_long','request_id','payment_status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function driver()
    {
        return $this->belongsTo('App\Driver');
    }

    
}
