@extends('plantillas.dashboard')

@section('contenido_dashboard')
    <div class="container">
		<div class="panel panel-default" style="margin-top: 60px;">
			<div class="panel-body">

				<ol class="breadcrumb">
					<li class="active">Inicio</li>
				</ol>

				<div class="page-header" style="margin-top: 0px; margin-bottom: 20px;">
				  	<h3>Bienvenido <small> / Inicio</small></h3>
				</div>

				@if (session('estatus'))
					<div class="alert alert-info alert-dismissible" role="alert">
					  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  	<strong>Mensaje:</strong> {{ session('estatus') }}
					</div>
				@endif

				<h5><strong>PRODUCTOS RECIENTEMENTE ANADIDOS</strong></h5>
				
				<table id="ultimos" class="table table-bordered" style="width:100%">
				    <thead>
				      	<tr>
				      		<th>Nombre</th>
					        <th>Modelo</th>
					        <th>Precio</th>
					        <th>Stock</th>
					        <th>Imagen</th>
				      	</tr>
				    </thead>
				    <tbody>
				    	@foreach ($ultimos_registrados as $indice => $ultimo)
							<tr>
					        	<td>{{ $ultimo->nombre }}</td>
					        	<td>{{ $ultimo->modelo }}</td>
					        	<td class="text-right"><strong>${{ $ultimo->precio }}</strong></td>
					        	<td class="text-right"><strong>{{ $ultimo->cantidad_piezas }}</strong></td>
					        	<td><img src="{{ $ultimo->imagen_color }}" width="100%" height="auto" /></td>
					      	</tr>
						@endforeach
				    </tbody>
				    </table>

				<hr>

				<h5><strong>PRODUCTOS CON PROMOCION</strong></h5>
				
				<table id="con_promocion" class="table table-bordered" style="width:100%">
				    <thead>
				      	<tr>
				      		<th>Nombre</th>
					        <th>Modelo</th>
					        <th>Precio</th>
					        <th>Stock</th>
					        <th>Imagen</th>
				      	</tr>
				    </thead>
				    <tbody>
				    	@foreach ($con_promocion as $indice => $promocion)
							<tr>
					        	<td>{{ $promocion->nombre }}</td>
					        	<td>{{ $promocion->modelo }}</td>
					        	<td class="text-right"><strong>${{ $promocion->precio }}</strong></td>
					        	<td class="text-right"><strong>{{ $promocion->cantidad_piezas }}</strong></td>
					        	<td><img src="{{ $promocion->imagen_color }}" width="100%" height="auto" /></td>
					      	</tr>
						@endforeach
				    </tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script>
	$(document).ready(function() {
		language =  {
						"sProcessing":     "Procesando...",
						"sLengthMenu":     "Mostrar _MENU_ registros",
						"sZeroRecords":    "No se encontraron resultados",
						"sEmptyTable":     "Ningún dato disponible en esta tabla",
						"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
						"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
						"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
						"sInfoPostFix":    "",
						"sSearch":         "Buscar:",
						"sUrl":            "",
						"sInfoThousands":  ",",
						"sLoadingRecords": "Cargando...",
						"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
						},
						"oAria": {
							"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
							"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
		}

		$('#ultimos').DataTable({
			"columnDefs": [
				{ "orderable": false, "width": "5%", "targets": 4 }
			],
            "language": language
        });

        $('#con_promocion').DataTable({
			"columnDefs": [
				{ "orderable": false, "width": "5%", "targets": 4 }
			],
            "language": language

        });
	});
</script>
@endpush

