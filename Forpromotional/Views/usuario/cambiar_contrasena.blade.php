@extends('plantillas.dashboard')

@section('contenido_dashboard')
    <div class="container">
		<div class="panel panel-default" style="margin-top: 60px;">
			<div class="panel-body">

				<ol class="breadcrumb">
					<li><a href="{{ route('auth_index') }}">Inicio</a></li>
					<li class="active">Editar</li>
				</ol>

				<div class="page-header" style="margin-top: 0px; margin-bottom: 20px;">	
				    <h3>Usuario <small> / Modificar contraseña</small></h3>
				</div>

				@if (session('error'))
					<div class="alert alert-warning alert-dismissible" role="alert">
					  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  	<strong>Mensaje:</strong> {{ session('error') }}
					</div>
				@endif

				<div class="alert alert-info alert-dismissible" role="alert">
				  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  	<strong>Mensaje:</strong> Se cerrara la sesión despues que haya modificado la contraseña.
				</div>

				<form class="contact-form row" action="{{ route('auth_guardar_contresena', ['id' => $usuario->id]) }}" method="POST">
					{{ method_field('POST') }}

					{{ csrf_field() }}

		            <div class="form-group col-md-4">
		            	<label for="name">Nombre</label>
		                <input type="text" name="name" id="name" readonly="readonly" class="form-control" value="{{ $usuario->name }}">
		            </div>
		            <div class="form-group col-md-4">
		            	<label for="email">Correo</label>
		                <input type="text" name="email" id="email" readonly="readonly" class="form-control" value="{{ $usuario->email }}">
		            </div>

					<div class="form-group col-md-12">
				    	<hr>
				    	<h5>CONTRASEÑA DEL USUARIO</h5>
				    </div>

				    <div class="form-group col-md-4">
					    <label for="contrasena_actual">Contraseña actual</label>
					    <input type="password" class="form-control" name="contrasena_actual" placeholder="Ingrese su contraseña actual">
					</div>

					<div class="form-group col-md-4">
					    <label for="contrasena_nueva">Contraseña nueva</label>
					    <input type="password" class="form-control" name="contrasena_nueva" placeholder="Ingrese su nueva contraseña">
					</div>

					<div class="form-group col-md-4">
					    <label for="repetir_contrasena">Repetir contraseña</label>
					    <input type="password" class="form-control" name="repetir_contrasena" placeholder="Repita su contraseña">
					</div>

		            <div class="form-group col-md-12">
		                <button type="submit" class="btn btn-primary pull-right">
		                	<i class="fa fa-floppy-o" aria-hidden="true"></i> 
		                	<strong>Modificar</strong>
		                </button>
		            </div>
		        </form>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script>
	$(document).ready(function() {
		
	});
</script>
@endpush