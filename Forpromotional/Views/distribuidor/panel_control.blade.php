@extends('plantillas.dashboard')

@section('contenido_dashboard')
    <div class="container">
		<div class="panel panel-default" style="margin-top: 60px;">
			<div class="panel-body">
				<form enctype="multipart/form-data" method="post" action="{{ route('auth_distribuidor_guardar_configuracion', ['id' => $distribuidor->user_id]) }}">
        			{{ csrf_field() }}

					<ol class="breadcrumb">
						<li><a href="{{ route('auth_index') }}">Inicio</a></li>
						<li class="active">Panel de control</li>
					</ol>

					<div class="page-header" style="margin-top: 0px; margin-bottom: 20px;">
					  	<h3>Distribuidor <small> / Panel de Control</small></h3>
					</div>

					<div class="row">
		                <div class="col-md-12">	           
		                	<h5><strong>INFORMACION DEL DISTRIBUIDOR</strong></h5>

		                	<div class="row">
					            <div class="form-group col-md-4">
								    <label for="name">Nombre del distribuidor</label>
								    <input type="text" class="form-control" name="name" id="name" value="@if(isset($distribuidor)) {{ $distribuidor->name }} @endif" placeholder="Ejemplo: Distribuidora Santiago ">
								</div>

								<div class="form-group col-md-4">
								    <label for="razon_social">Razon Social</label>
								    <input type="text" class="form-control" name="razon_social" id="razon_social" value="@if(isset($distribuidor)) {{ $distribuidor->razon_social }} @endif" placeholder="Ejemplo: Distribuidora Santiago, S.A. de C.V.">
								</div>

				                <div class="form-group col-md-4">
									<label for="rfc">RFC</label>
									<input type="text" class="form-control" name="rfc" id="rfc" value="@if(isset($distribuidor)) {{ $distribuidor->rfc }} @endif" placeholder="Ejemplo: AASM981007">
				                </div>
		                	</div>

		                	<div class="row">
					            <div class="form-group col-md-4">
								    <label for="dominio">Dominio</label>
								    <input type="text" class="form-control" readonly="readonly" name="dominio" id="dominio" value="@if(isset($distribuidor)) {{ $distribuidor->dominio }} @endif" placeholder="Ejemplo: distribuidora-santiago">
								</div>

					            <div class="form-group col-md-4">
								    <label for="numero_telefono">Telefono</label>
								    <input type="text" class="form-control" name="numero_telefono" id="numero_telefono" value="@if(isset($distribuidor)) {{ $distribuidor->numero_telefono }} @endif" placeholder="Ejemplo: 52 55 5685 4040">
								</div>

					            <div class="form-group col-md-4">
								    <label for="email">Correo</label>
								    <input type="text" class="form-control" name="email" id="email" value="@if(isset($distribuidor)) {{ $distribuidor->email }} @endif" placeholder="Ejemplo: ecommerce@santiago.mx">
								</div>
		                	</div>

		                	<div class="row">
		                		<div class="form-group col-md-12">
								    <label for="direccion">Direccion</label>
								    <textarea name="direccion" id="direccion" required="required" class="form-control" rows="2" placeholder="Ejemplo: Eje 8 Sur, Ermita Iztapalapa 1681, San Miguel, 09830 Ciudad de México, CDMX, México.">@if(isset($distribuidor)) {{ $distribuidor->direccion }} @endif</textarea>
								</div>
		                	</div>

		                	<h5><strong>PARA APLICAR A PRODUCTOS:</strong></h5>

		                	<div class="row">
					            <div class="form-group col-md-2">
								    <label for="name">Descuento</label>
								    <div class="input-group">
										<input type="text" onkeypress="return isNumberKey(event)" class="form-control" name="descuento" id="descuento" value="@if(isset($distribuidor)) {{ $distribuidor->descuento }} @endif" placeholder="Ejemplo: 20">
										<span class="input-group-addon">%</span>
									</div>
								</div>

								<div class="form-group col-md-2">
								    <label for="razon_social">Utilidad</label>
									<div class="input-group">
										<input type="text" onkeypress="return isNumberKey(event)" class="form-control" name="utilidad" id="utilidad" value="@if(isset($distribuidor)) {{ $distribuidor->utilidad }} @endif" placeholder="Ejemplo: 30">
										<span class="input-group-addon">%</span>
									</div>
								</div>
		                	</div>
		                </div>
		            </div>

		            <hr>

					<div class="row">
		                <div class="col-md-5">
		                	<h5><strong>LOGO</strong></h5>
		                    <div class="form-group">
		                    	<div style="margin-bottom: 10px;">
		                			<img src="@if(isset($distribuidor)) {{ URL::asset($distribuidor->logo) }} @else {{ URL::asset('images/pordefecto/logo.png') }} @endif" width="48%" height="auto" class="img-thumbnail img-responsive" alt="Logo del usuario">
		                		</div>
		                        <input type="file" class="form-control-file" name="logo" id="logo">
		                    </div>
		                </div>

		                <div class="col-md-7">
		                	<h5><strong>BANNER</strong></h5>
		                    <div class="form-group">
			                    <div style="margin-bottom: 10px;" >
			                    	<img src="@if(isset($distribuidor)) {{ URL::asset($distribuidor->banner) }} @else {{ URL::asset('images/pordefecto/banner.png') }} @endif" class="img-thumbnail img-responsive" alt="Banner del usuario">
			                	</div>
		                        <input type="file" class="form-control-file" name="banner" id="banner">
		                    </div>
		                </div>
		            </div>


		            <div class="row">
		            	<div class="col-md-12">
		            		<h5><strong>COLORES</strong></h5>
		                <div class="row">
		                	<div class="col-md-1">
						  		<div class="thumbnail">
						      		<img src="{{ URL::asset('images/colores/color-1.png') }}" alt="...">
						   		</div>

								<div class="radio">
								  	<label style="display: table;margin-right: auto; margin-left: auto;">
								    	<input type="radio" checked="checked" name="colores" value="0652DD">
								  	</label>
								</div>
						  	</div>

						  	<div class="col-md-1">
						  		<div class="thumbnail">
						      		<img src="{{ URL::asset('images/colores/color-2.png') }}" alt="...">
						   		</div>

								<div class="radio">
								  	<label style="display: table;margin-right: auto; margin-left: auto;">
								    	<input type="radio" name="colores" value="1B1464">
								  	</label>
								</div>
						  	</div>

						  	<div class="col-md-1">
						  		<div class="thumbnail">
						      		<img src="{{ URL::asset('images/colores/color-3.png') }}" alt="...">
						   		</div>

								<div class="radio">
								  	<label style="display: table;margin-right: auto; margin-left: auto;">
								    	<input type="radio" name="colores" value="EA2027">
								  	</label>
								</div>
						  	</div>

						  	<div class="col-md-1">
						  		<div class="thumbnail">
						      		<img src="{{ URL::asset('images/colores/color-4.png') }}" alt="...">
						   		</div>

								<div class="radio">
								  	<label style="display: table;margin-right: auto; margin-left: auto;">
								    	<input type="radio" name="colores" value="009432">
								  	</label>
								</div>
						  	</div>

						  	<div class="col-md-1">
						  		<div class="thumbnail">
						      		<img src="{{ URL::asset('images/colores/color-5.png') }}" alt="...">
						   		</div>

								<div class="radio">
								  	<label style="display: table;margin-right: auto; margin-left: auto;">
								    	<input type="radio" name="colores" value="FFC312">
								  	</label>
								</div>
						  	</div>
						</div>
					</div>
		            </div>	

		            <div class="row">
		                <div class="col-md-12">
		                    <div class="form-group">
		                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> <strong>Guardar</strong></button>
		                    </div>
		                </div>
		            </div>
	            </form>
			</div>
		</div>
	</div>
@endsection


@push('scripts')
<script>
	function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
           return false;

        return true;
    }

	$(document).ready(function() {
		$("input[name=colores][value=" + "'{{ $distribuidor->colores }}'" + "]").prop('checked', true);
	});
</script>
@endpush
