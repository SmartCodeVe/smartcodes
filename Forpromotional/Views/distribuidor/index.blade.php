@extends('plantillas.lista_distribuidores')

@section('contenido_lista_distribuidores')
    <div class="container">
		<div class="panel panel-default" style="margin-top: 30px;">
			<div class="panel-body">

				<div class="page-header" style="margin-top: 0px; margin-bottom: 20px;">
				  	<h3>Distribuidores <small> / paginas</small></h3>
				</div>

				<div class="row">
					@foreach ($distribuidores as $indice => $distribuidor)
				  	<div class="col-sm-4 col-md-4 col-lg-4">
				    	<div class="thumbnail">
				      		<img src="{{ $distribuidor->logo }}" style="width:100%" height="auto;" alt="...">
				      		<div class="caption">
					        	<h3 class="text-center">{{ $distribuidor->name }}</h3>
					        	<p class="text-center">{{ $distribuidor->razon_social }}</p>
					        	<p class="text-center"><a href="{{ route('index_producto', ['dominio' => $distribuidor->dominio]) }}" class="btn btn-primary" role="button"><i class="fa fa-share-square-o"></i> <strong>Ir al sitio</strong></a></p>
				      		</div>
				    	</div>
				  	</div>
				  	@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection
