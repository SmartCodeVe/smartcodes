@extends('plantillas.dashboard')

@section('contenido_dashboard')
    <div class="container">
		<div class="panel panel-default" style="margin-top: 60px;">
			<div class="panel-body">

				<ol class="breadcrumb">
					<li><a href="{{ route('auth_index') }}">Inicio</a></li>
					<li><a href="{{ route('auth_admin_listar_distribuidores') }}">Distribuidores</a></li>
					
					@if(isset($distribuidor))
					<li class="active">Editar</li>
					@else
					<li class="active">Nuevo</li>
					@endif
				</ol>

				<div class="page-header" style="margin-top: 0px; margin-bottom: 20px;">	
				@if(isset($distribuidor))
				    <h3>Producto <small> / Editar usuario</small></h3>
				@else
				    <h3>Producto <small> / Nuevo usuario</small></h3>
				@endif
				</div>


				@if(isset($distribuidor))
				    <form class="contact-form row" action="{{ route('auth_admin_guardar_distribuidor', ['user_id' => $distribuidor->user_id]) }}" method="POST">
					{{ method_field('PUT') }}
				@else
				    <form class="contact-form row" action="" method="POST">
					{{ method_field('POST') }}
				@endif

					{{ csrf_field() }}

		            <div class="form-group col-md-4">
		            	<label for="name">Nombre</label>
		                <input type="text" name="name" id="name" class="form-control" value="@if(isset($distribuidor)) {{ $distribuidor->name }} @endif" placeholder="Ejemplo: Administrador">
		            </div>
		            <div class="form-group col-md-4">
		            	<label for="email">Correo</label>
		                <input type="text" name="email" id="email" readonly="readonly" class="form-control" value="@if(isset($distribuidor)) {{ $distribuidor->email }} @endif" placeholder="Ejemplo: usuario@admin.com">
		            </div>

					<div class="form-group col-md-12">
				    	<hr>
				    	<h5>INFORMACION DEL DISTRIBUIDOR</h5>
				    </div>


				    <div class="form-group col-md-4">
					    <label for="razon_social">Razon Social</label>
					    <input type="text" class="form-control" name="razon_social" value="@if(isset($distribuidor)) {{ $distribuidor->razon_social }} @endif" placeholder="Ejemplo: Distribuidora Santiago, S.A. de C.V.">
					</div>

				    <div class="form-group col-md-2">
					    <label for="rfc">RFC</label>
					    <input type="text" class="form-control" name="rfc" value="@if(isset($distribuidor)) {{ $distribuidor->rfc }} @endif" placeholder="Ejemplo: AASM981007">
					</div>

					<div class="form-group col-md-2">
					    <label for="numero_telefono">Telefono</label>
					    <input type="text" class="form-control" name="numero_telefono" value="@if(isset($distribuidor)) {{ $distribuidor->numero_telefono }} @endif" placeholder="Ejemplo: +52 55 5685 4040">
					</div>

					<div class="form-group col-md-4">
					    <label for="dominio">Dominio</label>
					    <input type="text" class="form-control" name="dominio" value="@if(isset($distribuidor)) {{ $distribuidor->dominio }} @endif" placeholder="Ejemplo: distribuidora-santiago">
					</div>

					<div class="form-group col-md-12">
					    <label for="direccion">Direccion</label>
					    <textarea name="direccion" id="direccion" class="form-control" rows="2" placeholder="Ejemplo: Eje 8 Sur, Ermita Iztapalapa 1681, San Miguel, 09830 Ciudad de México, CDMX, México.">@if(isset($distribuidor)) {{ $distribuidor->direccion }} @endif</textarea>
					</div>

					<div class="form-group col-md-12">
					    <h5><strong>PARA APLICAR A PRODUCTOS:</strong></h5>
					</div>

		            <div class="form-group col-md-2">
					    <label for="name">Descuento</label>
					    <div class="input-group">
							<input type="text" onkeypress="return isNumberKey(event)" class="form-control" name="descuento" id="descuento" value="@if(isset($distribuidor)) {{ $distribuidor->descuento }} @endif" placeholder="Ejemplo: 20">
							<span class="input-group-addon">%</span>
						</div>
					</div>

					<div class="form-group col-md-2">
					    <label for="razon_social">Utilidad</label>
						<div class="input-group">
							<input type="text" onkeypress="return isNumberKey(event)" @if(isset($distribuidor)) readonly @endif class="form-control" name="utilidad" id="utilidad" value="@if(isset($distribuidor)) {{ $distribuidor->utilidad }} @endif" placeholder="Ejemplo: 30">
							<span class="input-group-addon">%</span>
						</div>
					</div>
                 
		            <div class="form-group col-md-12">
		                <button type="submit" name="submit" class="btn btn-primary pull-right">
		                	<i class="fa fa-paper-plane-o" aria-hidden="true"></i> 
		                	<strong>@if(isset($distribuidor)) Modificar @else Guardar @endif</strong>
		                </button>
		            </div>
		        </form>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script>
	$(document).ready(function() {
		
	});
</script>
@endpush