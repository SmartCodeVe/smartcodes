@extends('plantillas.dashboard')

@section('contenido_dashboard')
    <div class="container">
		<div class="panel panel-default" style="margin-top: 60px;">
			<div class="panel-body">

				<ol class="breadcrumb">
					<li><a href="{{ route('auth_index') }}">Inicio</a></li>
					<li class="active">Distribuidores</li>
				</ol>

				<div class="page-header" style="margin-top: 0px; margin-bottom: 20px;">
				  	<h3>Distribuidores <small> / lista</small></h3>
				</div>


				@if (session('estatus'))
					<div class="alert alert-info alert-dismissible" role="alert">
					  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  	<strong>Mensaje:</strong> {{ session('estatus') }}
					</div>
				@endif

				@if (session('error'))
					<div class="alert alert-warning alert-dismissible" role="alert">
					  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  	<strong>Mensaje:</strong> {{ session('error') }}
					</div>
				@endif

				<div class="row">
					<div class="col-sm-12">
						<div class="btn-group pull-right" role="group" aria-label="...">
						  	<a href="#" class="btn btn-primary" title="Nuevo distribuidor"><i class="fa fa-plus-circle" aria-hidden="true"></i> <strong>Nuevo</strong></a>
						</div>
					</div>
				</div>

				<br>

    			<div class="row">
					<div class="col-sm-12">
						<table id="distribuidores" class="table table-bordered table-condensed" style="width:100%">
						    <thead>
						        <tr>
						        	<th>Nombre</th>
						          	<th>Razon social</th>
						          	<th>RFC</th>
						          	<th>Descuento</th>
						          	<th>Dominio</th>
						          	<th>Direccion</th>
						          	<th>&nbsp;</th>
						        </tr>
						    </thead>
						    <tbody>
						    	@foreach ($distribuidores as $indice => $distribuidor)
						    		<tr>
						    			<td>{{ $distribuidor->name }}</td>
							          	<td>{{ $distribuidor->razon_social }}</td>
							          	<td>{{ $distribuidor->rfc }}</td>
							          	<th class="text-right">{{ $distribuidor->descuento }} %</th>
							          	<td>{{ $distribuidor->dominio }}</td>
							          	<td>{{ $distribuidor->direccion }}</td>
							          	<td class="text-center">
							          		<div>
							          			<a href="{{ route('auth_admin_editar_distribuidor', ['user_id' => $distribuidor->user_id]) }}" title="Editar este distribuidor"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;
							          			<a href="{{ route('auth_admin_eliminar_distribuidor', ['user_id' => $distribuidor->user_id]) }}" title="Eliminar este distribuidor"><i class="fa fa-trash" aria-hidden="true"></i></a>
							          		</div>
							          	</td>
							        </tr>
								@endforeach
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script>
	$(document).ready(function() {
		$('#distribuidores').DataTable({
			"columnDefs": [
				{ "orderable": false, "width": "5%", "targets": 6 }
			],
            "language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
            }
        });
	});
</script>
@endpush