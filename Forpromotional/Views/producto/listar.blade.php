@extends('plantillas.dashboard')

@section('contenido_dashboard')
    <div class="container">
		<div class="panel panel-default" style="margin-top: 60px;">
			<div class="panel-body">

				<ol class="breadcrumb">
					<li><a href="{{ route('auth_index') }}">Inicio</a></li>
					<li class="active">Productos</li>
				</ol>

				<div class="page-header" style="margin-top: 0px; margin-bottom: 20px;">
				  	<h3>Productos <small> / lista</small></h3>
				</div>

				@if (session('estatus'))
					<div class="alert alert-info alert-dismissible" role="alert">
					  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  	<strong>Mensaje:</strong> {{ session('estatus') }}
					</div>
				@endif

				@if (session('error'))
					<div class="alert alert-warning alert-dismissible" role="alert">
					  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  	<strong>Mensaje:</strong> {{ session('error') }}
					</div>
				@endif

				<div class="row">
					<div class="col-sm-12">
						<div class="btn-group pull-right" role="group" aria-label="...">
						  	<a href="{{ route('auth_admin_nuevo_producto') }}" class="btn btn-primary" title="Nuevo producto"><i class="fa fa-plus-circle" aria-hidden="true"></i> <strong>Nuevo</strong></a>
						</div>
					</div>
				</div>

				<br>

    			<div class="row">
					<div class="col-sm-12">
						<table id="productos" class="table table-bordered table-condensed" style="width:100%">
						    <thead>
						        <tr>
						          	<!-- <th>&nbsp;</th> -->
						          	<th>Marca</th>
						          	<th>Modelo</th>
						          	<th>Color</th>
						          	<th>Precio</th>
						          	<th>Categoria</th>
						          	<th>Sub Categoria</th>
						          	<th>&nbsp;</th>
						        </tr>
						    </thead>
						    <tbody>
						    	@foreach ($productos as $indice => $producto)
						    		<tr>
							          	<!-- <td class="text-center">
										    <input type="checkbox">
							          	</td> -->
							          	<td>{{ $producto->nombre }}</td>
							          	<td>{{ $producto->modelo }}</td>
							          	<td>{{ $producto->color }}</td>
							          	<th class="text-right">$ {{ $producto->precio }}</th>
							          	<td>{{ $producto->categoria }}</td>
							          	<td>{{ $producto->sub_categoria }}</td>
							          	<td class="text-center">
							          		<div>
							          			@role('admin')
					                                <a href="{{ route('auth_admin_editar_producto', ['id' => $producto->id]) }}" title="Editar este producto"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;&nbsp;
					                            @endrole
							          			<a href="{{ route('auth_admin_eliminar_producto', ['id' => $producto->id]) }}" title="Eliminar este producto"><i class="fa fa-trash" aria-hidden="true"></i></a>
							          		</div>
							          	</td>
							        </tr>
								@endforeach
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection


@push('scripts')
<script>
	$(document).ready(function() {
		$('#productos').DataTable({
			"columnDefs": [
				{ "orderable": false, "width": "5%", "targets": 6 }
			],
            "language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
					"sFirst":    "Primero",
					"sLast":     "Último",
					"sNext":     "Siguiente",
					"sPrevious": "Anterior"
				},
				"oAria": {
					"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
					"sSortDescending": ": Activar para ordenar la columna de manera descendente"
				}
            }
        });
	});
</script>
@endpush