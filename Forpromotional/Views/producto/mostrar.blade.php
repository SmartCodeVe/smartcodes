@extends('plantillas.catalogo')

@push('publicidad')
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
							<li data-target="#slider-carousel" data-slide-to="3"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active" style="margin-top: 15px; margin-bottom: 15px;">
								<div class="col-md-4 col-sm-12 clearfix">
									<h3>{{ $distribuidor->razon_social }}</h3>
									<p><small>{{ $distribuidor->direccion }}</small></p>
									<a href="{{ route('contacto_productos', ['dominio' => $distribuidor->dominio]) }}" class="btn btn-default get custom-color-background text-center">
										<i class="fa fa-phone" aria-hidden="true"></i> <strong>Informacion de contacto</strong>
									</a>
								</div>
								<div class="col-md-8 col-sm-12 clearfix" style="padding-top: 25px;">
									<img src="{{ URL::asset($distribuidor->banner) }}" class="img-responsive" style="float: none; margin: 0 auto;" width="110%" height="auto" alt="" />
								</div>
							</div>

							@foreach ($publicitarios as $indice => $publicitario)
								<div class="item" style="padding-top: 15px;">
									<div class="col-md-6 col-sm-12 clearfix">
										<h2>{{ $publicitario->nombre }} <small>{{ $publicitario->modelo }}</small></h2>
										<p><small>{{ $publicitario->descripcion }}</small></p>
										<p>
											<strong>
												${{ sprintf('%0.2f', floatval($publicitario->precio * 0.80)) }}&nbsp;
												<span class="text-danger"><s>${{ $publicitario->precio }}</s></span>
											</strong>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<img src="{{ URL::asset('images/product-details/rating.png') }}" alt="" class="product-rating" />
										</p>
										<a href="{{ route('mostrar_producto', ['dominio' => $distribuidor->dominio, 'id' => $publicitario->id]) }}" style="margin-bottom: 10px;" class="btn btn-default get custom-color-background text-center">
											<i class="fa fa-shopping-cart" aria-hidden="true"></i> <strong>Consultar producto</strong>
										</a>
									</div>
									<div class="col-md-6 col-sm-12 clearfix">
										<img src="{{ $publicitario->imagen_portada }}" class="girl img-responsive" style="float: none; margin: 0 auto;" width="50%" height="auto" alt="" />
										<!-- <img src="{{ URL::asset('images/home/pricing.png') }}" class="pricing" alt="" /> -->
									</div>

								</div>
							@endforeach
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
@endpush

@section('contenido_catalogo')
	<div class="product-details">
		<div class="col-sm-5">
			<div class="view-product">
				<img src="{{ $producto->imagen_portada }}" alt="" />
				<h3>ZOOM</h3>
			</div>
			<div id="similar-product" class="carousel slide" data-ride="carousel">
				<h5 class="text-center">Colores disponibles</h5>
				<div class="row">
					@foreach ($en_colores as $indice => $color)
					<div class="col-xs-6 col-md-4">
				    	<a href="#" class="thumbnail">
				      		<img src="{{ $color->imagen_color }}" title="{{ $color->color }}" alt="Color {{ strtolower($color->color) }}">
				    	</a>
				  	</div>
					@endforeach
				</div>
			</div>
		</div>

		<div class="col-sm-7">
			<div class="product-information"><!--/product-information-->
				<!-- <img src="{{ URL::asset('images/product-details/new.jpg') }}" class="newarrival" alt="" /> -->
				<h1>{{ $producto->nombre }} <small>{{ $producto->modelo }} </small></h1>
				<h4>{{ $producto->descripcion }}</h4>
				<p>Web ID: {{ $producto->id }}</p>
				<img src="{{ URL::asset('images/product-details/rating.png') }}" alt="" />
				<br>
				<span>
					<span class="price">${{ number_format($producto->total, 2, '.', '') }}</span>
					<label>Cantidad:</label>
					<input type="text" value="1" />
					<button type="button" class="btn btn-fefault cart">
						<i class="fa fa-cart-plus"></i> <strong>Cotizar</strong>
					</button>
				</span>
				<p><b>Disponibles:</b> {{ $producto->cantidad_piezas }}</p>
				<p><b>Material:</b> {{ $producto->material }}</p>
				<p><b>Medidas:</b> {{ $producto->medida_producto }}</p>
				<p><b>Marca:</b> <a href="{{ route('consultar_productos', ['dominio' => $distribuidor->dominio, 'marca' => $producto->nombre]) }}">{{ $producto->nombre }}</a></p>
				<!-- <a href=""><img src="{{ URL::asset('images/product-details/share.png') }}" class="share img-responsive"  alt="" /></a> -->
			</div><!--/product-information-->
		</div>
	</div>

	<div class="recommended_items">
		<h2 class="title text-center">Productos recomendados</h2>
		
		<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<?php $activo = False; $cerrado = False; $fila = 0; ?>

				@foreach ($recomendados as $indice => $recomendado)
					<?php $fila++; ?>

					@if ($fila == 1)
						@if ($activo == False)
							<div class="item active">
							<?php $activo = True; ?>
						@else
							<div class="item">
						@endif
					@endif

					@if ($fila < 4)
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="{{ $recomendado->imagen_portada }}" height="250px;" alt=""/>
										<h6>&nbsp;</h6>
										<p><strong>{{ $recomendado->nombre }}</strong> {{ $recomendado->modelo }}</p>
										<img src="{{ URL::asset('images/product-details/rating.png') }}" alt="" class="product-rating" />
										<a href="{{ route('mostrar_producto', ['dominio' => $distribuidor->dominio, 'id' => $recomendado->id]) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i> <strong>Añadir</strong></a>
									</div>
									<!-- <div class="product-overlay">
										<div class="overlay-content">
											<h2 style="color: white;">${{ $recomendado->precio }}</h2>
											<p>{{ $recomendado->descripcion }}</p>
											<a href="{{ route('mostrar_producto', ['dominio' => $distribuidor->dominio, 'id' => $recomendado->id]) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i> <strong>Añadir</strong></a>
										</div>
									</div> -->
								</div>
							</div>
						</div>
					@endif

					@if ($fila == 3)
						<?php $fila = 0; $cerrado = True; ?>
						</div>
					@endif
				@endforeach

				@if ($cerrado == False)
					</div>
				@endif
			</div>
			<a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
				<i class="fa fa-angle-left"></i>
			</a>
			<a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
				<i class="fa fa-angle-right"></i>
			</a>			
		</div>
	</div>
@endsection

@push('scripts')
<script>
	$(document).ready(function(){ });
</script>
@endpush