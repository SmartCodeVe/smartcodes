@extends('plantillas.catalogo')

@section('contenido_catalogo')
	<div class="features_items"><!--features_items-->
		<h2 class="title text-center">Detalle de productos</h2>
		@foreach ($productos as $indice => $producto)
			<div class="col-sm-4">
				<div class="product-image-wrapper">
					<div class="single-products">
						<div class="productinfo text-center">
							<img src="{{ $producto->imagen_color }}" height="250px;" alt="" />
							<h6>&nbsp;</h6>
							<p><strong>{{ $producto->nombre }}</strong> {{ $producto->modelo }}</p>
							<img src="{{ URL::asset('images/product-details/rating.png') }}" alt="" class="product-rating" />
							<a href="{{ route('mostrar_producto', ['dominio' => $distribuidor->dominio, 'id' => $producto->id ]) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i> <strong>Añadir</strong></a>
						</div>
						<!-- <div class="product-overlay">
							<div class="overlay-content">
								<h2 style="color: white;">${{ $producto->precio }}</h2>
								<p>{{ $producto->descripcion }}</p>
								<a href="{{ route('mostrar_producto', ['dominio' => $distribuidor->dominio, 'id' => $producto->id]) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i> <strong>Añadir</strong></a>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		@endforeach

		<div class="row">
			<div class="col-sm-12">
				{{ $productos->appends(['categoria' => $categoria, 'sub_categoria' => $sub_categoria, 'marca' => $marca])->links() }}
			</div>
		</div>
	</div>
@endsection