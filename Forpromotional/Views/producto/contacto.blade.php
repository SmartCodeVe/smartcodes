@extends('plantillas.main')

@push('styles')
<style type="text/css">
	.left-sidebar h2,
	.features_items h2,
	.recommended_items h2,
	.shop-menu ul li a:hover,
	.product-overlay .add-to-cart:hover,
	.product-information .price,
	.contact-info h2,
	.contact-form h2
	{
		color: #{{ $distribuidor->colores }};
	}

	#scrollUp,
	.btn,
	.view-product h3,
	.carousel .recommended-item-control,
	.pagination .page-link:hover,
	.add-to-cart:hover,
	a#scrollUp,
	a.recommended-item-control,
	.main-contact-form #submit
	{
		background: #{{ $distribuidor->colores }};
	}

	.product-overlay 
	{
		background: {{ $distribuidor->rgba }};
	}

	#contact-page .form-control:hover,
	#contact-page .form-control:focus
	{
		border-color: #{{ $distribuidor->colores }};
	}
</style>
@endpush

@section('contenido_main')
	<header id="header"><!--header-->
		<div class="header_top"><!--header_top-->
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-6 clearfix">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li>
									<a href="{{ route('index_producto', ['dominio' => $distribuidor->dominio]) }}">
										<i class="fa fa-home fa-lg"></i>
										<strong>{{ strtoupper($distribuidor->razon_social) }}</strong>&nbsp;&nbsp;
										+{{ $distribuidor->numero_telefono }}&nbsp;&nbsp;
										{{ strtoupper($distribuidor->razon_social) }}
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- <div class="col-lg-4 col-md-4 col-sm-6 clearfix">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div> -->
				</div>
			</div>
		</div><!--/header_top-->
		
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-12 clearfix">
						<div class="logo" style="text-align: center;">
							<!-- <a href="{{ route('index_producto', ['dominio' => $distribuidor->dominio]) }}">
								<img src="{{ URL::asset($distribuidor->logo) }}" width="10%" height="auto" class="img-thumbnail" alt="Logo de la Distribuidora" />
							</a> -->
						</div>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 clearfix">
						<div class="shop-menu clearfix pull-right">
							<ul class="nav navbar-nav">
								<!-- <li><a href=""><i class="fa fa-star"></i> Wishlist</a></li> -->
								<li><a href="{{ route('categorias_productos', ['dominio' => $distribuidor->dominio]) }}"><i class="fa fa-crosshairs"></i> CAT&#193;LOGOS</a></li>
								<li><a href="{{ route('consultar_productos', ['dominio' => $distribuidor->dominio]) }}"><i class="fa fa-shopping-cart"></i> PRODUCTOS NUEVOS</a></li>
								<!-- <li><a href="{{ route('contacto_productos', ['dominio' => $distribuidor->dominio]) }}"><i class="fa fa-user"></i> CONTACTO</a></li> -->
								<li><a href="{{ route('login') }}"><i class="fa fa-lock"></i> LOGIN</a></li>
								<li>
									<div class="search_box">
										<input type="text" placeholder="Buscar producto"/>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->
	</header><!--/header-->

	<br>
	
	<div id="contact-page" class="container">
    	<div class="bg">	
    		<div class="row">
    			<div class="col-sm-5">
	    			<div class="contact-info">
	    				<h2 class="title text-center">Contacto</h2>
						<div class="thumbnail">
					      	<img src="{{ URL::asset('images/callcenter.jpg') }}" class="img-responsive" alt="Contactanos">
					      	<div class="caption">
					        	<p>
					        		<address>
										<p>
											<i class="fa fa-home" aria-hidden="true"></i>&nbsp;
											{{ $distribuidor->razon_social }}
										</p>
										<p>
											<i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;
											{{ $distribuidor->direccion }}
										</p>
										<p>
											<i class="fa fa-phone" aria-hidden="true"></i>&nbsp;
											{{ $distribuidor->numero_telefono }}
										</p>
										<p>
											<i class="fa fa-envelope" aria-hidden="true"></i>&nbsp;
											{{ $distribuidor->email }}
										</p>
									</address>
					        	</p>

								<!-- <div class="social-networks">
									<ul>
										<li>
											<a href="#"><i class="fa fa-facebook"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-twitter"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-google-plus"></i></a>
										</li>
										<li>
											<a href="#"><i class="fa fa-youtube"></i></a>
										</li>
									</ul>
			    				</div> -->
					      	</div>
					    </div>
	    			</div>
    			</div>

	    		<!-- <div class="col-sm-8">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Envienos un correo</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" class="form-control" required="required" placeholder="Nombre">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" class="form-control" required="required" placeholder="Correo">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" class="form-control" required="required" placeholder="Telefono">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" class="form-control" required="required" placeholder="Tema">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Tu mensaje"></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <button type="submit" name="submit" id="submit" class="btn btn-primary pull-right"><i class="fa fa-paper-plane-o" aria-hidden="true"></i> <strong>Enviar</strong></button>
				            </div>
				        </form>
	    			</div>
	    		</div> -->    			
	    	</div>  
    	</div>	
    </div><!--/#contact-page-->
@endsection
