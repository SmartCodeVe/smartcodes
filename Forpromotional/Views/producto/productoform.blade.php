@extends('plantillas.dashboard')

@section('contenido_dashboard')
    <div class="container">
		<div class="panel panel-default" style="margin-top: 60px;">
			<div class="panel-body">

				<ol class="breadcrumb">
					<li><a href="{{ route('auth_index') }}">Inicio</a></li>
					<li><a href="{{ route('auth_admin_listar_productos') }}">Productos</a></li>
					
				@if(isset($producto))
				    <li class="active">Editar</li>
				@else
				    <li class="active">Nuevo</li>
				@endif
				</ol>

				<div class="page-header" style="margin-top: 0px; margin-bottom: 20px;">	
				@if(isset($producto))
				    <h3>Producto <small> / Editar producto</small></h3>
				@else
				    <h3>Producto <small> / Nuevo producto</small></h3>
				@endif
				</div>


				@if(isset($producto))
				    <form class="contact-form row" action="{{ route('auth_admin_guardar_producto', ['id' => $producto->id]) }}" method="POST">
					{{ method_field('PUT') }}
				@else
				    <form class="contact-form row" action="{{ route('auth_admin_guardar_producto') }}" method="POST">
					{{ method_field('POST') }}
				@endif

					{{ csrf_field() }}

		            <div class="form-group col-md-3">
		            	<label for="nombre">Marca</label>
		                <input type="text" name="nombre" id="nombre" class="form-control" value="@if(isset($producto)) {{ $producto->nombre }} @endif" placeholder="Ejemplo: YAREN">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="modelo">Modelo</label>
		                <input type="text" name="modelo" id="modelo" class="form-control" value="@if(isset($producto)) {{ $producto->modelo }} @endif" placeholder="Ejemplo: T 63">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="color">Color</label>
		                <input type="text" name="color" id="color" class="form-control" value="@if(isset($producto)) {{ $producto->color }} @endif" placeholder="Ejemplo: VERDE">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="precio">Precio</label>
		                <input type="text" onkeypress="return isNumber(event);" name="precio" id="precio" class="form-control" value="@if(isset($producto)) {{ $producto->precio }} @endif" placeholder="Ejemplo: 87.00">
		            </div>


		            <div class="form-group col-md-3">
		            	<label for="categoria">Categoria</label>
		                <input type="text" name="categoria" id="categoria" class="form-control" value="@if(isset($producto)) {{ $producto->categoria }} @endif" placeholder="Ejemplo: TERMOS">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="sub_categoria">Sub categoria</label>
		                <input type="text" name="sub_categoria" id="sub_categoria" class="form-control" value="@if(isset($producto)) {{ $producto->sub_categoria }} @endif" placeholder="Ejemplo: BEBIDAS">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="material">Material</label>
		                <input type="text" name="material" id="material" class="form-control" value="@if(isset($producto)) {{ $producto->material }} @endif" placeholder="Ejemplo: ACERO INOXIDABLE">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="medida_producto">Medidas del producto</label>
		                <input type="text" name="medida_producto" id="medida_producto" class="form-control" value="@if(isset($producto)) {{ $producto->medida_producto }} @endif" placeholder="Ejemplo: 6.0 x 23.0 cm">
		            </div>
		            

		            <div class="form-group col-md-3">
		            	<label for="capacidad">Capacidad</label>
		                <input type="text" name="capacidad" id="capacidad" class="form-control" value="@if(isset($producto)) {{ $producto->capacidad }} @endif" placeholder="Ejemplo: 380 ML">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="metodos_impresion">Metodos impresion</label>
		                <input type="text" name="metodos_impresion" id="metodos_impresion" class="form-control" value="@if(isset($producto)) {{ $producto->metodos_impresion }} @endif" placeholder="Ejemplo: SERIGRAFíA, TAMPOGRAFíA, GRABADO LASER">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="area_impresion">Area impresion</label>
		                <input type="text" name="area_impresion" id="area_impresion" class="form-control" value="@if(isset($producto)) {{ $producto->area_impresion }} @endif" placeholder="Ejemplo: 13 x 6 cm">
		            </div>
		            <div class="form-group col-md-3">
		            	<label for="tinta_azul">Tinta azul</label>
		                <input type="text" name="tinta_azul" id="tinta_azul" class="form-control" value="@if(isset($producto)) {{ $producto->tinta_azul }} @endif" placeholder="">
		            </div>


		            <div class="form-group col-md-2">
		            	<label for="peso_caja">Peso de caja</label>
		                <input type="text" onkeypress="return isNumber(event);" name="peso_caja" id="peso_caja" class="form-control" value="@if(isset($producto)) {{ $producto->peso_caja }} @endif" placeholder="Ejemplo: 14">
		            </div>
		            <div class="form-group col-md-2">
		            	<label for="alto">Alto</label>
		                <input type="text" onkeypress="return isNumber(event);" name="alto" id="alto" class="form-control" value="@if(isset($producto)) {{ $producto->alto }} @endif" placeholder="Ejemplo: 47">
		            </div>
		            <div class="form-group col-md-2">
		            	<label for="ancho">Ancho</label>
		                <input type="text" onkeypress="return isNumber(event);" name="ancho" id="ancho" class="form-control" value="@if(isset($producto)) {{ $producto->ancho }} @endif" placeholder="Ejemplo: 53">
		            </div>
		            <div class="form-group col-md-2">
		            	<label for="profundidad">Profundidad</label>
		                <input type="text" onkeypress="return isNumber(event);" name="profundidad" id="profundidad" class="form-control" value="@if(isset($producto)) {{ $producto->profundidad }} @endif" placeholder="Ejemplo: 43">
		            </div>
		            <div class="form-group col-md-2">
		            	<label for="cantidad_piezas">Cantidad de piezas</label>
		                <input type="text" onkeypress="return isNumber(event);" name="cantidad_piezas" id="cantidad_piezas" class="form-control" value="@if(isset($producto)) {{ $producto->cantidad_piezas }} @endif" placeholder="Ejemplo: 60">
		            </div>

		            <div class="form-group col-md-12">
		            	<label for="descripcion">Descripcion del producto</label>
		            	<textarea name="descripcion" id="descripcion" class="form-control" rows="2" placeholder="Ejemplo: Termo de acero inoxidable con acabado mate y correa para sujetar. Cap. 380 ml">@if(isset($producto)) {{ $producto->descripcion }} @endif</textarea>
		            </div>

		            <div class="form-group col-md-6">
		            	<label for="imagen_portada">URL de la imagen de portada</label>
		                <input type="text" name="imagen_portada" id="imagen_portada" class="form-control" value="@if(isset($producto)) {{ $producto->imagen_portada }} @endif" placeholder="Ejemplo: http://forpromotional.homelinux.com/eforpromotional/images/dummy/products/T%2063.jpg">
		            </div>
		            <div class="form-group col-md-6">
		            	<label for="imagen_color">URL de la imagen del producto</label>
		                <input type="text" name="imagen_color" id="imagen_color" class="form-control" value="@if(isset($producto)) {{ $producto->imagen_color }} @endif" placeholder="Ejemplo: http://forpromotional.homelinux.com/eforpromotional/images/dummy/img_color/T-63%20VERDEMO.jpg">
		            </div>

		                                
		            <div class="form-group col-md-12">
		                <button type="submit" name="submit" class="btn btn-primary pull-right">
		                	<i class="fa fa-paper-plane-o" aria-hidden="true"></i> 
		                	<strong>@if(isset($producto)) Modificar @else Guardar @endif</strong>
		                </button>
		            </div>
		        </form>
			</div>
		</div>
	</div>
@endsection

@push('scripts')
<script>
	function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    } 
</script>
@endpush
