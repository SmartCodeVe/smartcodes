@extends('plantillas.catalogo')

@push('publicidad')
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
							<li data-target="#slider-carousel" data-slide-to="3"></li>
						</ol>
						
						<div class="carousel-inner">
							<div class="item active" style="margin-top: 15px; margin-bottom: 15px;">
								<div class="col-md-4 col-sm-12 clearfix">
									<h3>{{ $distribuidor->razon_social }}</h3>
									<p><small>{{ $distribuidor->direccion }}</small></p>
									<a href="{{ route('contacto_productos', ['dominio' => $distribuidor->dominio]) }}" class="btn btn-default get custom-color-background text-center">
										<i class="fa fa-phone" aria-hidden="true"></i> <strong>Informacion de contacto</strong>
									</a>
								</div>
								<div class="col-md-8 col-sm-12 clearfix" style="padding-top: 25px;">
									<img src="{{ URL::asset($distribuidor->banner) }}" class="img-responsive" style="float: none; margin: 0 auto;" width="110%" height="auto" alt="" />
								</div>
							</div>

							@foreach ($publicitarios as $indice => $publicitario)
								<div class="item" style="padding-top: 15px;">
									<div class="col-md-6 col-sm-12 clearfix">
										<h2>{{ $publicitario->nombre }} <small>{{ $publicitario->modelo }}</small></h2>
										<p><small>{{ $publicitario->descripcion }}</small></p>
										<p>
											<strong>
												${{ sprintf('%0.2f', floatval($publicitario->precio * 0.80)) }}&nbsp;
												<span class="text-danger"><s>${{ $publicitario->precio }}</s></span>
											</strong>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<img src="{{ URL::asset('images/product-details/rating.png') }}" alt="" class="product-rating" />
										</p>
										<a href="{{ route('mostrar_producto', ['dominio' => $distribuidor->dominio, 'id' => $publicitario->id]) }}" style="margin-bottom: 10px;" class="btn btn-default get custom-color-background text-center">
											<i class="fa fa-shopping-cart" aria-hidden="true"></i> <strong>Consultar producto</strong>
										</a>
									</div>
									<div class="col-md-6 col-sm-12 clearfix">
										<img src="{{ $publicitario->imagen_portada }}" class="girl img-responsive" style="float: none; margin: 0 auto;" width="50%" height="auto" alt="" />
										<!-- <img src="{{ URL::asset('images/home/pricing.png') }}" class="pricing" alt="" /> -->
									</div>

								</div>
							@endforeach
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
@endpush

@section('contenido_catalogo')
	<div class="features_items"><!--features_items-->
		<h2 class="title text-center">Detalle de productos</h2>
		@foreach ($productos as $indice => $producto)
			<div class="col-sm-4">
				<div class="product-image-wrapper">
					<div class="single-products">
						<div class="productinfo text-center">
							<img src="{{ $producto->imagen_portada }}" height="250px;" alt="" />
							<h6>&nbsp;</h6>
							<p><strong>{{ $producto->nombre }}</strong> {{ $producto->modelo }}</p>
							<img src="{{ URL::asset('images/product-details/rating.png') }}" alt="" class="product-rating" />
							<a href="{{ route('mostrar_producto', ['dominio' => $distribuidor->dominio, 'id' => $producto->id ]) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i> <strong>Añadir</strong></a>
						</div>
						<!-- <div class="product-overlay">
							<div class="overlay-content">
								<h2 style="color: white;">${{ $producto->precio }}</h2>
								<p>{{ $producto->descripcion }}</p>
								<a href="{{ route('mostrar_producto', ['dominio' => $distribuidor->dominio, 'id' => $producto->id]) }}" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i> <strong>Añadir</strong></a>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		@endforeach
	</div>
@endsection
