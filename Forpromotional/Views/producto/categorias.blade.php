@extends('plantillas.main')

@push('styles')
<style type="text/css">
	.left-sidebar h2,
	.features_items h2,
	.recommended_items h2,
	.shop-menu ul li a:hover,
	.product-overlay .add-to-cart:hover,
	.product-information .price,
	.contact-info h2,
	.contact-form h2
	{
		color: #{{ $distribuidor->colores }};
	}

	#scrollUp,
	.btn,
	.view-product h3,
	.carousel .recommended-item-control,
	.pagination .page-link:hover,
	.add-to-cart:hover,
	a#scrollUp,
	a.recommended-item-control,
	.main-contact-form #submit
	{
		background: #{{ $distribuidor->colores }};
	}

	.product-overlay 
	{
		background: {{ $distribuidor->rgba }};
	}

	#contact-page .form-control:hover,
	#contact-page .form-control:focus
	{
		border-color: #{{ $distribuidor->colores }};
	}
</style>
@endpush

@section('contenido_main')
	<header id="header">
		<div class="header_top">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-8 col-sm-6 clearfix">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li>
									<a href="{{ route('index_producto', ['dominio' => $distribuidor->dominio]) }}">
										<i class="fa fa-home fa-lg"></i>
										<strong>{{ strtoupper($distribuidor->razon_social) }}</strong>&nbsp;&nbsp;
										+{{ $distribuidor->numero_telefono }}&nbsp;&nbsp;
										{{ strtoupper($distribuidor->razon_social) }}
									</a>
								</li>
							</ul>
						</div>
					</div>
					<!-- <div class="col-lg-4 col-md-4 col-sm-6 clearfix">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div> -->
				</div>
			</div>
		</div>
		
		<div class="header-middle">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-12 col-sm-12 clearfix">
						<div class="logo" style="text-align: center;">
							<!-- <a href="{{ route('index_producto', ['dominio' => $distribuidor->dominio]) }}">
								<img src="{{ URL::asset($distribuidor->logo) }}" width="10%" height="auto" alt="Logo de la Distribuidora" />
							</a> -->
						</div>
					</div>
					<div class="col-lg-8 col-md-12 col-sm-12 clearfix">
						<div class="shop-menu clearfix pull-right">
							<ul class="nav navbar-nav">
								<!-- <li><a href=""><i class="fa fa-star"></i> Wishlist</a></li> -->
								<li><a href="{{ route('categorias_productos', ['dominio' => $distribuidor->dominio]) }}"><i class="fa fa-crosshairs"></i> CAT&#193;LOGOS</a></li>
								<li><a href="{{ route('consultar_productos', ['dominio' => $distribuidor->dominio]) }}"><i class="fa fa-shopping-cart"></i> PRODUCTOS NUEVOS</a></li>
								<!-- <li><a href="{{ route('contacto_productos', ['dominio' => $distribuidor->dominio]) }}"><i class="fa fa-user"></i> CONTACTO</a></li> -->
								<li><a href="{{ route('login') }}"><i class="fa fa-lock"></i> LOGIN</a></li>
								<li>
									<div class="search_box">
										<input type="text" placeholder="Buscar producto"/>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<br>
	
	<div id="contact-page" class="container">
		<div class="features_items"><!--features_items-->
			<h2 class="title text-center">CAT&#193;LOGOS</h2>
			@foreach ($categorias as $indice => $categoria)
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
					<div class="product-image-wrapper">
						<div class="single-products">
							<div class="productinfo text-center">
								<img src="{{ $categoria->imagen_portada }}" height="250px;" alt="" />
								<h6>&nbsp;</h6>
								<p>{{ $categoria->categoria }} (<b>{{ $categoria->cantidad }}</b>)</p>
							</div>
							<!-- <div class="product-overlay">
								<div class="overlay-content">
									<h2 style="color: white;">{{ $categoria->categoria }}</h2>
									<a href="{{ route('consultar_productos', ['dominio' => $distribuidor->dominio, 'categoria' => $categoria->categoria]) }}" class="btn btn-default add-to-cart">
										<i class="fa fa-hand-pointer-o"></i> <strong>Consultar</strong>
									</a>
								</div>
							</div> -->
						</div>
					</div>
				</div>
			@endforeach
    	</div>
    </div>
@endsection
