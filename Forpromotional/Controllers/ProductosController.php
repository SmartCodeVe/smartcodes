<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductosCatalogosImport;
use App\Imports\ProductosListaPreciosImport;
use Illuminate\Http\Request;
use App\Producto;
use App\Distribuidor;

class ProductosController extends Controller 
{
    public function __construct()
    {
        $num_filas = DB::table('productos')->count();

        if ($num_filas === 0) 
        {
            return $this->importar();
        }
    }


    public function importar() 
    {
        Excel::import(new ProductosCatalogosImport, 'catalogo.xlsx');
        Excel::import(new ProductosListaPreciosImport, 'lista_precios.xlsx');
        
        return redirect('/')->with('success', 'Todo los productos fueron cargados.');
    }


    public function buscar($dominio, Request $request)
    {
        $distribuidor = Distribuidor::consultar($dominio);
        $termino = $request->input('buscar_producto');

        if (count($distribuidor) > 0) 
        {
            $productos = Producto::query()
                                ->where('nombre', 'LIKE', "%{$termino}%")
                                ->orWhere('modelo', 'LIKE', "%{$termino}%")
                                ->orWhere('descripcion', 'LIKE', "%{$termino}%")
                                ->paginate(9);


            return view('producto.busqueda', [
                'categorias' => Producto::categorias(),
                'marcas' => Producto::marcas(),
                'productos' => $productos,
                'distribuidor' => $distribuidor[0],
                'termino' => $termino
            ]);
        }
        else 
        {
            return redirect()->route('404');
        }
    }


    public function index($dominio)
    {
        $distribuidor = Distribuidor::consultar($dominio);

        if (count($distribuidor) > 0) 
        {
            return view('producto.index', [
                'categorias' => Producto::categorias(),
                'marcas' => Producto::marcas(),
                'publicitarios' => Producto::publicitarios(),
                'productos' => Producto::productos(),
                'distribuidor' => $distribuidor[0],
            ]);
        }
        else 
        {
            return redirect()->route('404');
        }
    }


    public function contacto($dominio)
    {
        $distribuidor = Distribuidor::consultar($dominio);

        if (count($distribuidor) > 0) 
        {
            return view('producto.contacto', [
                'distribuidor' => $distribuidor[0],
            ]);
        }
        else 
        {
            return redirect()->route('404');
        }
    }


    public function categoriasAgrupadas($dominio)
    {
        $distribuidor = Distribuidor::consultar($dominio);

        if (count($distribuidor) > 0) 
        {
            return view('producto.categorias', [
                'categorias' => Producto::categoriasAgrupadas(),
                'distribuidor' => $distribuidor[0],
            ]);
        }
        else 
        {
            return redirect()->route('404');
        }
    }


    public function marcasAgrupadas()
    {
        // Producto::marcasAgrupadas();

        // return view('producto.marcas', [
        //     'marcas' => Producto::marcasAgrupadas(),
        // ]);

        return "marcasAgrupadas";
    }


    public function consultar($dominio, Request $request)
    {
        $distribuidor = Distribuidor::consultar($dominio);

        if (count($distribuidor) > 0) 
        {
            $categoria = $request->input('categoria');
            $sub_categoria = $request->input('sub_categoria');
            $marca = $request->input('marca');

            return view('producto.consultar', [
                'categorias' => Producto::categorias(),
                'marcas' => Producto::marcas(),
                'productos' => Producto::consultar($categoria, $sub_categoria, $marca),
                'distribuidor' => $distribuidor[0],
                'categoria' => $categoria,
                'sub_categoria' => $sub_categoria,
                'marca' => $marca,
            ]);
        }
        else 
        {
            return redirect()->route('404');
        }
    }


    public function mostrar($dominio, Request $request)
    {
        $distribuidor = Distribuidor::consultar($dominio);

        if (count($distribuidor) > 0) 
        {
            $_id = $request->input('id');
            $producto = Producto::where('id', $_id)->select([
                    "id", "nombre", "modelo", "descripcion", "promocion",  
                    "precio", "categoria", "sub_categoria", "imagen_portada",
                    "medida_producto", "material", "cantidad_piezas"
            ])->first();

            $producto_array = $producto->toArray();

            // * Precio Lista: $2.50
            // * Descuento: 20%
            // * Costo a distribuidora: $2.50 * 0.2 = $2.00
            // * Margen de utilidad: 30%
            // * Precio a clientes: $2.00 (costo a distribuidora) * 0.7 (diferencia de 100 - 30 que es el margen de utilidad) = 2.85


            $descuento = floatval($distribuidor[0]->descuento);
            $descuento = ($descuento / 100);
            
            if ($producto['promocion'] == 0 && $descuento > 0) 
            {
                $precio_lista = floatval($producto_array['precio']);
                $costo_distribuidora = $precio_lista - ($precio_lista * $descuento);
            }
            else if ($producto['promocion'] == 1 or $descuento == 0.00) 
            {
                $costo_distribuidora = floatval($producto_array['precio']);
            }

            $margen_utilidad = floatval($distribuidor[0]->utilidad);
            $diferencia = (100.00 - $margen_utilidad) / 100;
            $total = floatval($costo_distribuidora * $diferencia);
            $producto->total = round($total, 2);

            $recomendados = Producto::recomendados(
                $producto->categoria, $producto->sub_categoria, $producto->id
            );

            return view('producto.mostrar', [
                'categorias' => Producto::categorias(),
                'marcas' => Producto::marcas(),
                'publicitarios' => Producto::publicitarios(),
                'en_colores' => Producto::enColores($producto->nombre, $producto->modelo),
                'distribuidor' => $distribuidor[0],
                'recomendados' => $recomendados,
                'producto' => $producto,
            ]);
        }
        else 
        {
            return redirect()->route('404');
        }
    }



    public function listar_productos(Request $request)
    {
        $productos = Producto::select([
                'id', 'nombre', 'modelo', 'color', 'precio', 'categoria', 'sub_categoria'
            ])->orderBy('nombre', 'ASC')->get();

        return view('producto.listar', compact("productos"));
    }


    public function nuevo_producto()
    {
        return view('producto.productoform');
    }


    public function editar_producto($id)
    {
        return view('producto.productoform', [
            'producto' => Producto::find($id),
        ]);
    }


    public function guardar_producto($id = null, Request $request)
    {
        try
        {
            if ($request->isMethod('post')) 
            {
                $producto = new Producto;
            }
            else 
            {
                $producto = Producto::find($id);
            }

            $producto->modelo_color = $request->modelo . " " . $request->color;
            $producto->nombre = $request->nombre; 
            $producto->modelo = $request->modelo;
            $producto->descripcion = $request->descripcion;
            $producto->color = $request->color;
            $producto->precio = floatval($request->precio);
            $producto->medida_producto = $request->medida_producto;
            $producto->material = $request->material;
            $producto->categoria = $request->categoria;
            $producto->sub_categoria = $request->sub_categoria;
            $producto->tinta_azul = $request->tinta_azul;
            $producto->capacidad = $request->capacidad;
            $producto->metodos_impresion = $request->metodos_impresion;
            $producto->area_impresion = $request->area_impresion;
            $producto->peso_caja = intval($request->peso_caja);
            $producto->alto = intval($request->alto);
            $producto->ancho = intval($request->ancho);
            $producto->profundidad = intval($request->profundidad);
            $producto->cantidad_piezas = $request->cantidad_piezas;
            $producto->imagen_portada = $request->imagen_portada;
            $producto->imagen_color = $request->imagen_color;
            $producto->save();

            return redirect()->route('auth_admin_listar_productos')->with('estatus', 'Producto guardado con exito.');
        }
        catch(\Exception $e)
        {
            return redirect()->route('auth_admin_listar_productos')->with('error', 'Fallo la operacion.');
        }
    }


    public function eliminar_producto($id)
    {
        try
        {
            $producto = Producto::find($id);
            $producto->delete();

            return redirect()->route('auth_admin_listar_productos')->with('estatus', 'Producto eliminado con exito.');
        }
        catch(\Exception $e)
        {
            return redirect()->route('auth_admin_listar_productos')->with('error', 'Fallo la operacion.');
        }
    }
}