<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Distribuidor;

class DistribuidoresController extends Controller
{
    public function index()
    {
        $distribuidores = DB::table('users')
            ->join('distribuidores', 'users.id', '=', 'distribuidores.user_id')
            ->select('users.name', 'users.email', 'distribuidores.*')
            ->get();

        return view('distribuidor.index', [
            'distribuidores' => $distribuidores,
        ]);
    }
    

    public function panel_control()
    {
        $this->middleware('auth');

        $distribuidor = DB::table('users')
            ->join('distribuidores', 'users.id', '=', 'distribuidores.user_id')
            ->select('users.name', 'users.email', 'distribuidores.*')
            ->where('user_id', Auth::user()->id)
            ->first();

        return view('distribuidor.panel_control', [
            'distribuidor' => $distribuidor,
        ]);
    }


    public function guardar_configuracion($id, Request $request)
    {
        $this->middleware('auth');
        
        $logoname = null;
        $bannername = null;

        foreach (array('logo', 'banner') as $img) 
        {
            if ($request->hasFile($img)) 
            {
                $imgfile = $request->file($img);
                $extension = $imgfile->getClientOriginalExtension();
                $filename = $imgfile->getFilename().'.'.$extension;
                Storage::disk('public')->put($filename,  File::get($imgfile));

                if ($img == 'logo') $logoname = 'images/'.$filename;
                else $bannername = 'images/'.$filename;
            }
        }


        $usuario = User::find($id);
        $usuario->name = $request->name;
        $usuario->email = $request->email;
        $usuario->save();

        $rgba_colors = array(
            '0652DD' => 'rgba(6, 82, 224, 0.8)',
            '1B1464' => 'rgba(27, 20, 100, 0.8)',
            'EA2027' => 'rgba(234, 32, 39, 0.8)',
            '009432' => 'rgba(0, 148, 50, 0.8)',
            'FFC312' => 'rgba(255, 195, 18, 0.8)',
        );

        $rgba = $rgba_colors[$request->colores];


        $sql = "UPDATE distribuidores 
                SET
                    utilidad = '{$request->utilidad}', 
                    descuento = '{$request->descuento}',
                    rfc = '{$request->rfc}', 
                    razon_social = '{$request->razon_social}',
                    direccion = '{$request->direccion}',
                    numero_telefono = '{$request->numero_telefono}',
                    colores = '{$request->colores}',
                    rgba = '{$rgba}'";

        if ($logoname != null) {
            $sql .= ", logo = '{$logoname}' ";
        }

        if ($bannername != null) {
            $sql .= ", banner = '{$bannername}' ";
        }

        $sql .= " WHERE user_id = {$id}";
        DB::update($sql);
        
        return redirect()->route('auth_index')->with('estatus', 'Configuracion del distribuidor guardada con exito.');
    }
    

    public function listar_distribuidores()
    {
        $distribuidores = DB::table('users')
            ->join('distribuidores', 'users.id', '=', 'distribuidores.user_id')
            ->select('users.name', 'users.email', 'distribuidores.*')
            ->get();

        return view('distribuidor.listar_distribuidores', compact("distribuidores"));
    }


    public function editar_distribuidor($user_id)
    {
        $distribuidor = DB::table('users')
            ->join('distribuidores', 'users.id', '=', 'distribuidores.user_id')
            ->select('users.name', 'users.email', 'distribuidores.*')
            ->where('user_id', $user_id)
            ->first();

        return view('distribuidor.distribuidor_form', [
            'distribuidor' => $distribuidor,
        ]);
    }


    public function guardar_distribuidor($user_id = null, Request $request)
    {
        try {
            if ($request->isMethod('post')) 
            {
                $usuario = new User;
            }
            else 
            {
                $usuario = User::where("id", $user_id)->first();
            }

            $usuario->name = $request->name; 
            $usuario->email = $request->email;
            $usuario->save();

            $descuento = $request->descuento; 
            $utilidad = $request->utilidad;
            $razon_social = $request->razon_social; 
            $rfc = $request->rfc;
            $dominio = $request->dominio;
            $numero_telefono = $request->numero_telefono;
            $direccion = $request->direccion;

            if ($request->isMethod('post')) 
            {
                $distribuidor = new Distribuidor;
                $distribuidor->user_id = $usuario->id;
                $distribuidor->descuento = $descuento;
                $distribuidor->razon_social = $razon_social; 
                $distribuidor->rfc = $rfc;
                $distribuidor->dominio = $dominio;
                $distribuidor->numero_telefono = $numero_telefono;
                $distribuidor->direccion = $direccion;
                $distribuidor->save();

                return redirect()->route('auth_admin_listar_distribuidores')->with('estatus', 'Distribuidor guardado con exito.');
            }
            else 
            {
                $sql = "UPDATE distribuidores 
                        SET 
                            descuento = '{$descuento}', 
                            utilidad = '{$utilidad}', 
                            razon_social = '{$razon_social}', 
                            rfc = '{$rfc}', 
                            dominio = '{$dominio}', 
                            numero_telefono = '{$numero_telefono}', 
                            direccion = '{$direccion}'
                        WHERE user_id = '{$usuario->id}'";
                DB::update($sql);
            }

            return redirect()->route('auth_admin_listar_distribuidores')->with('estatus', 'Distribuidor guardado con exito.');
        }
        catch(\Exception $e) {
            return redirect()->route('auth_admin_listar_distribuidores')->with('estatus', 'Distribuidor guardado con exito.');
        }
    }


    public function eliminar_distribuidor($user_id)
    {
        try
        {
            $distribuidor = Distribuidor::find($user_id);
            $distribuidor->delete();

            return redirect()->route('auth_admin_listar_distribuidores')->with('estatus', 'Producto eliminado con exito.');
        }
        catch(\Exception $e)
        {
            return redirect()->route('auth_admin_listar_distribuidores')->with('error', 'Fallo la operacion.');
        }
    }
}
