<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;
use App\Distribuidor;
use App\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    

    public function index()
    {
        return view('usuario.index', [
            'distribuidor' => User::info_distribuidor(),
            'ultimos_registrados' => Producto::ultimos_registrados(),
            'con_promocion' => Producto::con_promocion(),
        ]);
    }


    public function cambiar_contrasena()
    {
        $usuario = User::where('id', Auth::user()->id)
                    ->select('id', 'name', 'email', 'password')
                    ->first();

        return view('usuario.cambiar_contrasena', 
            ['usuario' => $usuario]
        );
    }


    public function guardar_contrasena($id, Request $request)
    {
        $email = trim($request->email);
        $con_actual = trim($request->contrasena_actual);
        $con_nueva = trim($request->contrasena_nueva);
        $con_repetida = trim($request->repetir_contrasena);
        $con_encritada = bcrypt($con_nueva);


        $usuario = User::where('email', $email)->first();

        if ($usuario == null) 
        {
            return redirect()->route('auth_cambiar_contresena')->with('error', 'Disculpe, este usuario no existe.');
        }


        if (Hash::check($con_actual, $usuario->password)) 
        {
            if ($con_nueva === $con_repetida) 
            {
                $sql = "UPDATE users 
                        SET password = '{$con_encritada}' 
                        WHERE email = '{$email}'
                        AND id = {$id}";
                DB::update($sql);

                Auth::logout();
                return redirect('/login');
            }
            else
            {
                return redirect()->route('auth_cambiar_contresena')->with('error', 'Las nuevas contraseñas no coinciden.');
            }
        }
        else
        {
            return redirect()->route('auth_cambiar_contresena')->with('error', 'Ingrese la contraseña del usuario correcta.');
        }
    }
}
