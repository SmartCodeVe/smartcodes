<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Distribuidor extends Model
{
    protected $table = 'distribuidores';

    protected $guarded = [];

    public static function consultar($dominio)
    {
        $sql = "SELECT 
        			* 
        		FROM distribuidores
                JOIN users ON distribuidores.user_id = users.id
        		WHERE dominio = '{$dominio}'";
        return DB::select($sql);
    }
}
