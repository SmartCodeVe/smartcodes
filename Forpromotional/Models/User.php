<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Permissions\HasPermissionsTrait;

class User extends Authenticatable
{
    use Notifiable, HasPermissionsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function distribuidor()
    {
        return $this->hasOne('App\Distribuidor');
    }

    static public function info_distribuidor()
    {
        $id = Auth::user()->id;

        $sql = "SELECT 
                    users.name, distribuidores.logo, distribuidores.dominio
                FROM distribuidores
                JOIN users ON distribuidores.user_id = users.id
                WHERE user_id = '{$id}'";
        $datos = DB::select($sql);

        if (count($datos) > 0) 
        {   
            return $datos[0];
        }
        else
        {
            return null;
        }
    }
}
