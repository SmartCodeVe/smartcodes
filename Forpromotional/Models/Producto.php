<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Producto extends Model
{
    protected $table = 'productos';

    protected $fillable = [
        'modelo_color', 'nombre', 'modelo', 'descripcion', 'color',
        'precio', 'medida_producto', 'material', 'categoria', 'sub_categoria',
        'tinta_azul', 'capacidad', 'metodos_impresion', 'area_impresion', 'peso_caja',
        'alto', 'ancho', 'profundidad', 'cantidad_piezas', 'imagen_portada', 'imagen_color',
    ];

    public $timestamps = true;

    protected $guarded = [];


    public static function categorias()
    {
        $categorias = array();

        $sql = "SELECT 
                    DISTINCT categoria, sub_categoria 
                FROM productos 
                ORDER BY categoria ASC";
        $datos = DB::select($sql);

        foreach ($datos as $indice => $fila) 
        {
            $categoria = trim($fila->categoria);
            $sub_categoria = trim($fila->sub_categoria);
            $en_arreglo = array_key_exists($categoria, $categorias);

            if (!$en_arreglo) 
            {
                $categorias[$categoria] = array();
                self::agregarCategoria($categorias, $categoria, $sub_categoria);
            }
            else 
            {
                self::agregarCategoria($categorias, $categoria, $sub_categoria);
            }
        }

        return $categorias;
    }

    public static function agregarCategoria(&$categorias, $categoria, $sub_categoria)
    {
        array_push($categorias[$categoria], $sub_categoria);
    }



    public static function marcas()
    {
        $sql = "SELECT 
                    COUNT(*) AS cantidad, nombre 
                FROM productos 
                GROUP BY nombre 
                ORDER BY cantidad DESC
                LIMIT 10";
        return DB::select($sql);
    }



    public static function consultar($categoria, $sub_categoria, $marca)
    {
        $columnas = array('id', 'nombre', 'modelo', 'descripcion', 'precio', 'imagen_color');


        if (!empty($categoria))
        { 
            $filtro = array("columna" => "categoria", "termino" => $categoria);
        }
        else if (!empty($sub_categoria)) 
        {
            $filtro = array("columna" => "sub_categoria", "termino" => $sub_categoria);
        }
        else if (!empty($marca))
        {
            $filtro = array("columna" => "nombre", "termino" => $marca);
        }
        

        if (empty($categoria) && empty($sub_categoria) && empty($marca)) 
        {
            $productos = Producto::select($columnas)->paginate(9);
        }
        else 
        {
            $productos = Producto::where($filtro["columna"], $filtro["termino"])
                ->select($columnas)->paginate(9);
        }


        return $productos;
    }



    public static function publicitarios()
    {
        $cantidad = 3;
        $publicitarios = array();

        $sql = "SELECT 
                    id, nombre, modelo, descripcion, precio, imagen_portada 
                FROM productos";
        $datos = DB::select($sql);

        for ($k = 0; $k < $cantidad; $k++) 
        { 
            $indice = random_int(1, count($datos));
            $registro = $datos[$indice-1];

            if (isset($registro))
            {
                array_push($publicitarios, $registro);
            }
            else 
            {
                $k -= 1;
            }
        }

        return $publicitarios;
    }



    public static function productos()
    {
        $cantidad = 6;
        $productos = array();
        $agregados = array();

        $sql = "SELECT 
                    id, nombre, modelo, descripcion, precio, imagen_portada 
                FROM productos";
        $datos = DB::select($sql);
        $total = count($datos);

        for ($i = 0; $i < $cantidad; $i++) 
        { 
            $indice = random_int(1, $total);
            $registro = $datos[$indice];

            $key = $registro->nombre . "-" . $registro->modelo;

            if (isset($registro) && !in_array($key, $agregados))
            {
                array_push($productos, $registro);
                array_push($agregados, $key);
            }
            else 
            {
                $i -= 1;
            }
        }

        return $productos;
    }



    public static function recomendados($categoria, $sub_categoria, $id_producto)
    {
        $cantidad = 9;
        $recomendados = array();

        $sql = "SELECT 
                    id, nombre, modelo, descripcion, precio, imagen_portada 
                FROM productos 
                WHERE categoria = '{$categoria}' 
                AND sub_categoria = '{$sub_categoria}'
                AND id NOT IN ('{$id_producto}')";
        $datos = DB::select($sql);
        $total = count($datos);


        for ($i = 0; $i < $total; $i++) 
        { 
            $registro = $datos[$i];
            $clave = "{$registro->nombre} {$registro->modelo}";

            if (!array_key_exists($clave, $recomendados)) 
            { 
                $recomendados[$clave] = $registro; 
            }

            if (count($recomendados) == $cantidad)
            {
                return $recomendados;
            }
        }


        $faltantes = ($cantidad - count($recomendados));

        if ($faltantes > 0) 
        {
            $sql = "SELECT 
                    id, nombre, modelo, descripcion, precio, imagen_portada 
                FROM productos
                WHERE id NOT IN ('{$id_producto}')
                LIMIT {$faltantes}";
            $datos = DB::select($sql);

            foreach ($datos as $indice => $fila) 
            {
                $clave = "{$fila->nombre} {$fila->modelo}";
                $recomendados[$clave] = $fila; 
            }
        }

        return $recomendados;
    }
   


    public static function enColores($nombre, $modelo)
    {
        $sql = "SELECT 
                    nombre, modelo, color, imagen_color 
                FROM `productos` 
                WHERE nombre = '{$nombre}' 
                AND modelo = '{$modelo}'
                GROUP BY nombre, modelo, color, imagen_color";
        return DB::select($sql);
    }



    public static function categoriasAgrupadas()
    {
        $sql = "SELECT 
                    COUNT(id) AS cantidad, categoria
                FROM productos
                GROUP BY categoria 
                ORDER BY categoria ASC";
        $datos = DB::select($sql);

        foreach ($datos as $indice => &$categoria) 
        {
            $_categoria = $categoria->categoria;

            $sql = "SELECT 
                        imagen_portada 
                    FROM productos
                    WHERE categoria = '{$_categoria}'
                    LIMIT 1, 1";
            $portada = DB::select($sql)[0];
            $categoria->imagen_portada = $portada->imagen_portada;
        }

        return $datos;
    }


    public static function ultimos_registrados()
    {
        $sql = "SELECT 
                    id, nombre, modelo, precio, cantidad_piezas, imagen_color
                    FROM productos
                    ORDER BY created_at DESC 
                    LIMIT 5";
        return DB::select($sql);
    }

    public static function con_promocion()
    {
        $sql = "SELECT 
                    id, nombre, modelo, precio, cantidad_piezas, imagen_color
                FROM productos
                WHERE promocion = 1";
        return DB::select($sql);
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($producto) {
            $producto->{$producto->getKeyName()} = (string) Str::uuid();
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }
}
