@extends('backend.layouts.app')

@section('title', __('labels.backend.access.permissions.management') . ' | ' . __('labels.backend.access.permissions.create'))

@section('content')
{{ html()->form('POST', route('admin.movimientos.store'))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
    <div id="movimientos">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Movimientos
                                <small class="text-muted">Crear</small>
                            </h5>
                            
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal">
                               <div class="row">
                                <div class="form-group col-md-5 col-md-push-3">
                                    {{ html()->label('Tipo Operacion')->for('tipo_operacion_id') }}
                                    <select v-model="operacion" class="form-control" name="tipo_operacion_id" id="tipo_operacion_id">
                                     <option value="0">Seleccion Operación</option>
                                        @foreach($tipoOpera as $key=>$value)
                                           <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>

                                </div><!--form-group-->

                                </div>
                            <div v-if="operacion!=0">
                                <div class="row">
                                <div v-if="operacion==1 || operacion==2"  class="form-group col-md-5">
                                    {{ html()->label('Factura')->for('nro_factura') }}
                                    <input type="text" class="form-control" name="nro_factura" required>
                                    
                                </div>
                               
                         
                                </div>
                            
                                <div class="row">
                                  <div class="form-group col-md-5">
                                    {{ html()->label('Fecha')->for('fecha') }}
                                    <input type="date" class="form-control" name="fecha" value="{{date("Y-m-d")}}">
                                  </div>
                                  <div v-if="operacion!=4"  class="form-group col-md-5 col-md-push-1">
                                    {{ html()->label('Bodegas')->for('bodega_id') }}
                                     {{ html()->select('bodega_id', $bodegas,null)
                                          ->placeholder('Seleccione Bodega', false)
                                          ->class('form-control')
                                          ->required()
                                          ->id('bodega_id') }}
                                  </div>  
                                </div>

                                <div v-if="operacion==4"  class="row">
                                  <div class="form-group col-md-5">
                                    {{ html()->label('Bodega Origen')->for('bodegan_origen') }}
                                    {{ html()->select('bodega_origen', $bodegas,null)
                                          ->placeholder('Seleccione Bodega Origen', false)
                                          ->class('form-control')
                                          ->required()
                                          ->id('bodega_origen') }}
                                  </div>
                                  <div class="form-group col-md-5 col-md-push-1">
                                    {{ html()->label('Bodega Destino')->for('bodega_destino') }}
                                     {{ html()->select('bodega_destino', $bodegas,null)
                                          ->placeholder('Seleccione Bodega', false)
                                          ->class('form-control')
                                          ->required()
                                          ->id('bodega_destino') }}
                                  </div>  
                                </div>


                                <div class="row">
                                <div class="form-group col-md-5">
                                    {{ html()->label('Producto')->for('producto') }}
                                    {{ html()->select('producto_id', $productos,null)
                                          ->placeholder('Seleccione Producto', false)
                                          ->class('form-control')
                                          ->required()
                                          ->id('producto_id') }}
                          
                                </div>
                                <div class="form-group col-md-5 col-md-push-1">
                                     {{ html()->label('Cantidad')->for('cantidad') }}
                                        <input type="number" class="form-control" min="1" step="any" name="cantidad" required>
                                     
                                </div><!--form-group-->
                         
                                </div>


                                <div class="row">
                                  <div v-if="operacion==2" class="form-group col-md-5">
                                    {{ html()->label('Cliente')->for('cliente_id') }}
                                    {{ html()->select('cliente_id', $clientes,null)
                                          ->placeholder('Seleccione Cliente', false)
                                          ->class('form-control')
                                          ->required()
                                          ->id('cliente_id') }}
                                  </div>
                                  <div v-if="operacion==1" class="form-group col-md-5">
                                    {{ html()->label('Proveedores')->for('proveedor_id') }}
                                     {{ html()->select('proveedor_id', $proveedores,null)
                                          ->placeholder('Seleccione Proveedor', false)
                                          ->class('form-control')
                                          ->required()
                                          ->id('proveedor_id') }}
                                  </div>  
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-12">
                                    {{ html()->label('Comentarios')->for('comentarios') }}
                                     <textarea class="summernote" id="comentarios" name="comentarios" title="Descripcion"></textarea>
                                    </div>
                                 </div>
                               </div>
                                                         

                           
                            <div v-if="operacion!=0" class="mail-body text-right tooltip-demo">
                                                        
                                <a class="btn btn-white btn-sm" href="{{route('admin.movimientos.index')}}" >@lang('buttons.general.cancel')</a>
                                <button class="btn btn-sm btn-primary" type="submit">Crear Movimiento</button>
                                
                            </div>
                            



                                
                            </form>
                        </div>
                    </div>
                
</div>

{{ html()->form()->close() }}
@endsection

@section('scripts')

<script>
    $('#comentarios').summernote({
        height: 150,
    });
    var movimiento=new Vue({
        el:"#movimientos",
        data: {
            operacion: "0",
        }
    });

</script>

@endsection


