@extends('backend.layouts.app')

@section('title',  'Cuarteles| Editar' )

@section('content')
{{ html()->modelForm($cuartel, 'PATCH', route('admin.cuarteles.update',$cuartel))->class('form-horizontal')->open() }}
<div>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>
                Administrador de Cuarteles
                <small class="text-muted">
                    Editar Cuarteles
                </small>
            </h5>
        </div>
        <div class="ibox-content">
            <form class="form-horizontal">
                <div class="row">
                    <div class="form-group col-md-5">
                        {{ html()->label(__('validation.attributes.backend.access.permissions.name'))
                                ->for('name') }}

                                  
                                        {{ html()->text('nombre')
                                            ->class('form-control')
                                            ->placeholder(__('validation.attributes.backend.access.permissions.name'))
                                            ->attribute('maxlength', 191)
                                            ->required()
                                            ->autofocus() }}
                    </div>
                    <div class="form-group col-md-5 col-md-push-1">
                        {{ html()->label('Campos')->for('campo_id') }}
                                     {{ html()->select('campo_id', $campos,null)
                                        ->placeholder('Seleccione Campo', false)
                                        ->class('form-control')
                                        ->required()
                                        ->id('campo_id') }}
                    </div>
                    <!--form-group-->
                </div>
                <div class="row">
                    <div class="form-group col-md-5 ">
                        {{ html()->label('Provincias')->for('provincia_id') }}
                                     {{ html()->select('provincia_id', $provincias,null)
                                        ->placeholder('Seleccione Provincia', false)
                                        ->class('form-control')
                                        ->required()
                                        ->id('provincia_id') }}
                    </div>
                    <!--form-group-->
                    <div class="form-group col-md-5 col-md-push-1">
                        {{ html()->label('Comunas')->for('comuna_id') }}
                                     {{ html()->select('comuna_id', $comuna)
                                          ->placeholder('Seleccione Comuna', false)
                                          ->class('form-control')
                                          ->required()
                                          ->id('comuna_id') }}
                    </div>
                    <!--form-group-->
                </div>
                <div class="row">
                    <div class="form-group col-md-5">
                        {{ html()->label('Tamaño (Hectareas)')
                                            ->for('name') }}
                        <input class="form-control" id="tamanno" min="0" name="tamanno" required="" step="any" type="number" value="{{$cuartel->tamanno}}">
                        </input>
                        <a class="btn btn-round btn-info" data-target="#mapa" data-toggle="modal" title="Ver en Google Maps" type="button">
                            <i class="fas fa-map">
                            </i>
                        </a>
                    </div>
                    <div class="form-group col-md-5 col-md-push-1">
                        {{ html()->label('Tipo Cultivo')->for('campo_id') }}
                                     {{ html()->select('tipo_cultivo_id', $tipoCultivos,null)
                                        ->placeholder('Seleccione Tipo de Cultivo', false)
                                        ->class('form-control')
                                        ->required()
                                        ->id('tipo_cultivo_id') }}
                    </div>
                    <!--form-group-->
                </div>
                <div class="row">
                    <div class="form-group col-md-5">
                        <input name="propio" type="checkbox" value="1" {{isset($cuartel->
                            propio)&&$cuartel->propio==1?'checked':''}} /> Propio
                        </input>
                    </div>
                    <!--form-group-->
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        {{ html()->label('Descripción')
                                            ->for('name') }}
                        <textarea class="summernote" id="descripcion" name="descripcion" title="Descripcion">
                            {{$cuartel->descripcion}}
                        </textarea>
                    </div>
                </div>
                <div class="mail-body text-right tooltip-demo">
                    <a class="btn btn-white btn-sm" href="{{route('admin.cuarteles.index')}}">
                        @lang('buttons.general.cancel')
                    </a>
                   
                </div>
            </form>
        </div>
    </div>
</div>
{{ html()->form()->close() }}
@endsection
@section('scripts')
<script>
    var pacContainerInitialized = false; 
   $('#pac-input').keypress(function() { 
        if (!pacContainerInitialized) { 
            $('.pac-container').css('z-index','9999'); 
            pacContainerInitialized = true; 
        } 
    });

    $('#descripcion').summernote({
        height: 150,
    });
 
</script>
@endsection
@include('backend.cuarteles.mapshow')
