<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model
{

    protected $table    = 'movimientos';
    protected $dates = ['fecha'];
    protected $fillable = [
        'id',
        'fecha',
        'nro_factura',
        'tipo_operacion_id',
        'tipo_movimiento_id',
        'stock_id',
        'cantidad',
        'cliente_proveedor_id',
        'user_id',
        'comentarios',
    ];



    public function tipo_operacion()
    {
        return $this->belongsTo('App\Models\TipoOperacion','tipo_operacion_id','id');
    }

    public function tipo_movimiento()
    {
        return $this->belongsTo('App\Models\TipoMovimiento','tipo_movimiento_id','id');
    }

    public function stock()
    {
        return $this->belongsTo('App\Models\Stock','stock_id','id');
    }

    public function cliente_proveedor()
    {
        return $this->belongsTo('App\Models\ClienteProveedor','cliente_proveedor_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User','user_id','id');
    }




}
