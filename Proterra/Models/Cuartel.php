<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Attribute\CuartelAttribute;

class Cuartel extends Model
{
    use CuartelAttribute;
    protected $table = 'cuarteles';
    protected $fillable = [
        'id',
        'nombre',
        'propio',
        'provincia_id',
        'comuna_id',
        'descripcion',
        'campo_id',
        'tipo_cultivo_id',
        'coordenadas',
        'ubiq_lat',
        'ubiq_lng',
        'tamanno'
    ];


    public function provincia()
    {
        return $this->belongsTo('App\Models\Provincia');
    }

    public function comuna()
    {
        return $this->belongsTo('App\Models\Comuna');
    }

    public function campo()
    {
        return $this->belongsTo('App\Models\Campo');
    }

    public function tipoCultivo()
    {
        return $this->belongsTo('App\Models\TipoCultivo');
    }


    public function actividades() {

        return $this->belongsToMany('App\Models\Actividad','actividad_cuartel','cuartel_id','actividad_id');
    }





}
